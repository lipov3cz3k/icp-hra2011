﻿ICP - Hra 2011

Člověče, nezlob se!


Tomáš Ondrouch

Tomáš Lipovský


Překlad hry: 				make
Odstranění dočasných souborů:		make clean
Spuštění přeložené hry:			make run
Spuštění serveru:			make runserver
Vytvoření implementační dokumentace:	make doxygen
Vytvoření balíčku:			make pack

Poznámka: Před vytvořením implementační dokumentace doporučuji spustit make clean


Úvodní obrazovka
Při spuštění je na výběr v hlavním menu několik možností - načíst rozehranou hru, vytvořit novou lokální a nebo síťovou hru. Dále je možné si v položce Settings vybrat kořenové adresáře, z kterých bude načítat uložené hry, typy hracích polí či vybrat vzhled nebo automatické přepínání módu počítače. Z pohledu síťové hry je tu možné nastavit IP adresu a port serveru.

Lokální hra
U lokální hry je možné si vybrat hrací pole a nastavit hráče. U hráčů si krom hry jednoho uživatele proti počítačům, můžeme vybrat i další hráče a hrát tak může u jednoho PC více lidí. U počítače je možný výběr několika módů, podle kterých se budou řídit jeho tahy. Je na výběr preferování nejdelšího skoku, skoku do domečku, nasazení a náhodného módu. Při hození kostkou se nám zvýrazní políčka, na která mohou hráči táhnout. Hru je možné kdykoliv uložit a později načíst.

Síťová hra
U síťové hry je nejprve potřeba spustit server, který běží na portu 55555. Po spuštění server načte ze souboru seznam uživatelů, kteří jsou na něm zaregistrováni. Pokud uživatel nemá na serveru účet, může se po připojení k serveru zaregistrovat a pokud již má účet, tak je možné se k serveru přihlásit. Při registraci je každému hráči vytvořena na serveru samostatná složka, do které ukládá své rozehrané hry. Poté některý z připojených uživatelů musí založit novou hru/načíst síťovou ze serveru a k této hře se následně mohou připojit další uživatelé.