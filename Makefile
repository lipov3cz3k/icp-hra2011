#######################################################################
## Project: Člověče nezlob se
## Authors: Tomáš Lipovský (xlipov02)
##          Tomáš Ondrouch (xondro06)
## Date:   8. 5. 2011
##
## Description: Projekt modifikované hry člověče nezlob se do předmětu
##              Seminář C++ (ICP) v jazyce C++ s podporou toolkitu QT4
#######################################################################

NAME=xzaple27

.PHONY: clean run runserver pack hra server

all: hra server

hra:
	$(MAKE) --directory=src hra

server:
	$(MAKE) --directory=src server

clean:
	$(MAKE) --directory=src clean
	rm -f -r ./doc/*

run:
	$(MAKE) --directory=src run

runserver:
	$(MAKE) --directory=src runserver

pack:
	tar cvzf $(NAME).tar.gz src doc examples

doxygen:
	doxygen Doxyfile