/** @file server2011.cpp
 *  @brief Modul pro komunikaci s klientem
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#include "stdafx.h"
#include "server2011.h"
#include <QtNetwork>


server2011::server2011(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	this->setWindowTitle(QString(tr("Do not get angry, man (v0.23 ALPHA)")));
	mainLayout = new QHBoxLayout(ui.centralWidget);
	loginFile = "loginlist.txt";
	port = 55555;
	setStatus(WAITNEWGAME);
	addr = QHostAddress(QHostAddress::Any);
	QFile fLogin(loginFile);
	if (!fLogin.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	QString loginData;
	
	ui.loglabel->setText("USERS:\n");
	while (!fLogin.atEnd()){
		loginData = fLogin.readLine();
		if (loginData != ""){
			QStringList loginArray = loginData.split(' ');
			QString user = loginArray.takeFirst().toLower();
			QString pass = loginArray.takeFirst();
			pass.remove(pass.size() - 1, 1);
			users.insert(user, pass);
			ui.loglabel->setText(ui.loglabel->text() + user + "\n");
		}
	}
	fLogin.close();
}

void server2011::on_pushStartServer_clicked(){

	server = new QTcpServer(this);
	connect(server, SIGNAL(newConnection()), this, SLOT(handleNewConnection()));
	QTimer::singleShot(60*1000, this, SLOT(purgeLog()));

	server->setProxy(QNetworkProxy::NoProxy);
	server->setMaxPendingConnections(50);
	bool listening = server->listen(addr, port);

	QString ipport = addr.toString() + ":" + QString::number(port);
	if(!listening)
	{
		ui.statuslabel->setText(tr("couldn't bind to %1, quitting�").arg(ipport) + "\n");
		QTimer::singleShot(0, qApp, SLOT(quit()));
	} else
	{
		ui.statuslabel->setText(tr("listening on %1").arg(ipport) + "\n");
		ui.pushStartServer->setEnabled(false);
	}
}

void server2011::socketWrite(QTcpSocket* socket, QByteArray data)
{
	socket->write(data);
	socket->flush();
}

void server2011::handleNewConnection()
{
	QTcpSocket* socket = server->nextPendingConnection();
	if(!socket)
		return;

	connect(socket, SIGNAL(readyRead()), SLOT(reply()));
	connect(socket, SIGNAL(disconnected()), SLOT(gotDisconnected()));
	this->socketWrite(socket, "LOGIN");
}


void server2011::reply()
{
	QTcpSocket* socket = qobject_cast<QTcpSocket*>(this->sender());

	if(log.value(socket->peerAddress().toString(), 0) >= 10)
		this->socketWrite(socket, "TOOMANYWRONGLOGINS");

	QByteArray rawdata = socket->readAll();
	QList<QByteArray> args = rawdata.split(' ');
	QString command = args.takeFirst();

	ui.loglabel->setText(ui.loglabel->text() + command + "\n");
	QString user = logins.value(socket);
	qDebug() << rawdata;
	QString pass;
	if(command == "LOGIN")
	{
		if (!user.isEmpty())
		{
			this->socketWrite(socket, "ALREADYLOGGED");
			return;
		}

		// heslo
		QString pass;
		if (args.size() == 2)
		{
			user = args.at(0).toLower();
			pass = args.at(1);
		}
		this->login(socket, user, pass);
		return;
	}

	if (user.isEmpty())
	{
		socket->disconnectFromHost();

		QString ip(socket->peerAddress().toString());
	}
	if(command == "REGISTER"){
		if (args.size() == 2)
		{
			user = args.at(0).toLower();
			pass = args.at(1);
		}
		this->regUser(socket, user, pass);
	}
	else
	if (command == "NEWGAME"){
		countOfPlayers = args.at(0).toInt();
		this->newGame(socket, user);
	}
	else
	if (command == "JOINGAME"){
		int position = args.at(0).toInt();
		this->joinGame(socket, user, position);
	}
	else
	if (command == "JUMP"){
		this->jumpToPiece(socket, user, rawdata);
	}else
	if (command == "STARTGAME"){
		QList<QByteArray> lines = rawdata.split('\n');
		lines.removeFirst();
		this->startGame(socket, lines);
	}
	else
	if (command == "LEAVE"){
		this->leaveGame(socket, user);
	}
	else
	if (command == "GAMELIST"){
		this->sendGameList(socket, user);
	}
	else
	if (command == "SAVEGAME"){
		QList<QByteArray> lines = rawdata.split('\n');
		lines.removeFirst();
		QString fileName = args.takeFirst();
		this->saveGame(socket, user, lines, fileName);
	}
	else
	if (command == "LOADGAME"){
		QString fileName = args.takeFirst();
		this->loadGame(socket, user, fileName);
	}
	else
	if (command == "GAMECANCELED"){
		this->cancelGame();
	}
	else
	if (command == "ENDGAME"){
		this->endGame();
	}else
	if (command == "CHECKGAME"){
		this->checkGame(socket);
	}else
	if (command == "LOGOUT"){
		this->logOut(socket);
	}
	else
	{
		this->socketWrite(socket, "INVALIDCMD");
		return;
	}
}


void server2011::login(QTcpSocket* socket, const QString& user,
		      const QString& pass)
{
	const QString realpass = users.value(user);

	if(!user.isEmpty() && !realpass.isEmpty() && pass == realpass)
	{
		bool alreadyLogged = logins.values().contains(user);
		if(alreadyLogged)
		{
			this->socketWrite(socket, "ALREADYLOGGED");
			return;
		}

		if(alreadyLogged)
		{
			QList<QTcpSocket*> keys = logins.keys();
			for(int i=0; i<keys.size(); i++)
				if(logins.value(keys.at(i)) == user)
				{
					QTcpSocket* oldSocket = keys.at(i);
					oldSocket->disconnectFromHost();

					logins.remove(oldSocket);
					break;
				}
		}
		this->socketWrite(socket, "LOGGED");
		logins.insert(socket, user);


	} else
	{
		this->socketWrite(socket, "WRONGLOGIN");
		QString ip(socket->peerAddress().toString());
		log.insert(ip, log.value(ip, 0)+1);

	}
}

void server2011::regUser(QTcpSocket* socket, QString user, QString pass){

	QByteArray reply;
	QString realPass = users.value(user);
	if(!user.isEmpty() && realPass.isEmpty() && !pass.isEmpty()){
		
		reply += "REGISTERED";
		this->socketWrite(socket, reply);
		users.insert(user, pass);
		QFile fLogin(loginFile);
		fLogin.open(QIODevice::Append);
		reply.clear();
		reply += user + " " + pass + "\n";
		fLogin.write(reply);
		QDir saves;
		saves.mkpath(tr("Resources\\save\\%1").arg(user));
		fLogin.close();
	}
	else {
		reply += "NOTREGED";
		this->socketWrite(socket, reply);
		socket->disconnectFromHost();
	}
	ui.loglabel->setText(ui.loglabel->text() + reply);
}

void server2011::newGame(QTcpSocket* socket, QString user){
	QByteArray reply;
	if (getStatus() == WAITNEWGAME){
		setStatus(NEWGAME);
		gamePlayers.insert(socket, qMakePair(user, 1));
		this->createPlayerList(reply);
		QList <QTcpSocket*> sockets = logins.keys();
		this->sendToAllPlayers(sockets, reply);
		creatorSocket = socket;
		ui.loglabel->setText(ui.loglabel->text() + reply);
	}
	else {
		reply += "CANNOTCREATE \n";
		this->socketWrite(socket, reply);
		ui.loglabel->setText(ui.loglabel->text() + reply);
	}

}

void server2011::joinGame(QTcpSocket* socket, QString user, int position){
	QByteArray reply;
	if (getStatus() == NEWGAME && gamePlayers.size() != countOfPlayers){
		gamePlayers.insert(socket, qMakePair(user, position));
		QHash<QTcpSocket*, QString>::iterator i;
		this->createPlayerList(reply);
		QList <QTcpSocket*> sockets = logins.keys();
		this->sendToAllPlayers(sockets, reply);
		
		ui.loglabel->setText(ui.loglabel->text() + reply);
	}
	else {
		reply += "CANNOTJOIN";
		this->socketWrite(socket, reply);
		ui.loglabel->setText(ui.loglabel->text() + reply);
	}

}

void server2011::leaveGame(QTcpSocket* socket, QString user){
	QByteArray reply;
	QList <QTcpSocket*> sockets = logins.keys();		
	if (getStatus() == NEWGAME && gamePlayers.size() > 0){
		if (socket == creatorSocket){
			reply += "GAMECANCELED";
			this->sendToAllPlayers(sockets, reply);
			gamePlayers.clear();
			setStatus(WAITNEWGAME);
		}
		else {
			gamePlayers.remove(socket);
			this->createPlayerList(reply);
			this->sendToAllPlayers(sockets, reply);
		}
		ui.loglabel->setText(ui.loglabel->text() + reply);
	}
}

void server2011::logOut(QTcpSocket* socket){
	gamePlayers.remove(socket);
	logins.remove(socket);
	socket->disconnectFromHost();
}

void server2011::cancelGame(){
	QByteArray reply;
	if (getStatus() == NEWGAME && gamePlayers.size() > 0){
		reply += "GAMECANCELED";
		QList <QTcpSocket*> sockets = logins.keys();
		sendToAllPlayers(sockets, reply);
		gamePlayers.clear();
		setStatus(WAITNEWGAME);
	}
	ui.loglabel->setText(ui.loglabel->text() + reply);

}

void server2011::startGame(QTcpSocket* socket, QList<QByteArray> fileLines){
	if (getStatus() == NEWGAME){
		setStatus(GAMERUN);
		QByteArray reply;
		reply += "GAMESTARTED \n";
		QList<QByteArray>::iterator j;
		for (j = fileLines.begin(); j != fileLines.end(); ++j){
			reply += *j;
		}

		QHash<QTcpSocket*, QPair<QString, int> >::iterator i;
		for (i = gamePlayers.begin(); i != gamePlayers.end(); ++i){
			if (i.key() != socket)
				this->socketWrite(i.key(), reply);
		}
	}
}

void server2011::checkGame(QTcpSocket* socket){
	QByteArray reply;
	if (getStatus() == WAITNEWGAME)
		reply += "WAITNEWGAME";
	else {
		reply += "CHECKPLAYERS \n";
		this->createPlayerList(reply);
	}
	socketWrite(socket, reply);
	ui.loglabel->setText(ui.loglabel->text() + reply);
}

void server2011::endGame(){
	if (getStatus() == GAMERUN){
		setStatus(WAITNEWGAME);
		gamePlayers.clear();
	}
}

void server2011::saveGame(QTcpSocket* socket, QString user, QList<QByteArray> fileLines, QString fileName){

	QByteArray reply;
	
	QList<QByteArray>::iterator j;
	QByteArray output;
	for (j = fileLines.begin(); j != fileLines.end(); ++j){
		output += *j;
	}
	QFile gameFile(tr("Resources\\save\\%1\\%2").arg(user, fileName));
	if (!gameFile.open(QIODevice::WriteOnly | QIODevice::Text))
		reply += "NOTSAVED";
	else {
		reply += "SAVED";
		gameFile.write(output);
		gameFile.close();
	}

	this->socketWrite(socket, reply);
}

void server2011::loadGame(QTcpSocket* socket, QString user, QString fileName){
	QByteArray reply;
	if (getStatus() == WAITNEWGAME){
		QFile inputFile(fileName);
		if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
			reply += "CANNOTCREATE";
		else {
			setStatus(NEWGAME);
			gamePlayers.insert(socket, qMakePair(user, 1));
			reply += "GAMELOADED \n";
			QString input = inputFile.readAll();
			reply += input;
		}
	}
	else {
		reply += "CANNOTCREATE";
	}
	this->socketWrite(socket, reply);
	ui.loglabel->setText(ui.loglabel->text() + reply);
}


void server2011::jumpToPiece(QTcpSocket* socket, QString user, QByteArray message){
	if (getStatus() == GAMERUN){
		QHash<QTcpSocket*, QPair<QString, int> >::iterator i;
		for (i = gamePlayers.begin(); i != gamePlayers.end(); ++i){
			if (i.value().first != user)		
				this->socketWrite(i.key(), message);
		}
	}
}

void server2011::sendGameList(QTcpSocket* socket, QString user){
	QByteArray reply;
	QStringList savedGames;
	findSavedGames(savedGames, user);
	if(savedGames.empty()){
		reply += "GLEMPTY";
	}
	else {
		reply += "LISTING \n";
		QStringList::iterator i;
		for (i = savedGames.begin(); i != savedGames.end(); ++i){
			reply += *i;
		}
	}
	this->socketWrite(socket, reply);
	ui.loglabel->setText(ui.loglabel->text() + reply);
}

void server2011::findSavedGames(QStringList &savedGames, QString user){
	savedGames.clear();
	QDir gameDir(tr("Resources\\save\\%1").arg(user));
	QStringList filter;
    filter << "*.xml";
    gameDir.setNameFilters(filter);
	savedGames = gameDir.entryList();
	
}


void server2011::gotDisconnected()
{

	QTcpSocket* socket = qobject_cast<QTcpSocket*>( this->sender() );

	QString user = logins.value(socket);

	if (user.isEmpty())
		user = "*not logged*";
	else {
		logins.remove(socket);
		gamePlayers.remove(socket);
		QByteArray reply;
		this->createPlayerList(reply);
		QList <QTcpSocket*> allSockets;
		this->sendToAllPlayers(allSockets, reply);

	}
	socket->deleteLater();
}

void server2011::setStatus(int newStatus){
	status = newStatus;
}

int server2011::getStatus(){
	return status;
}

void server2011::createPlayerList(QByteArray &message){
	
	message += "PLAYERLIST ";
	QHash<QTcpSocket*, QPair<QString, int> >::iterator i;
	for (i = gamePlayers.begin(); i != gamePlayers.end(); ++i){
		message.append("\n"+i.value().first+" "+QString::number(i.value().second));
	}
}

void server2011::sendToAllPlayers(QList<QTcpSocket*> allSockets, QByteArray message){
	QList<QTcpSocket*>::iterator i;
	for (i = allSockets.begin(); i != allSockets.end(); ++i){
		this->socketWrite(*i, message);
	}
}

void server2011::purgeLog()
{
	log.clear();
}



server2011::~server2011()
{

}
