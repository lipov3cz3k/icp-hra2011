/** @file server2011.h
 *  @brief Modul pro komunikaci s klientem
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#ifndef SERVER2011_H
#define SERVER2011_H

#include <QtGui/QMainWindow>
#include "ui_server2011.h"
#include <QObject>
#include <QAbstractSocket>
#include <QList>
#include <QPair>
#include <QHostAddress>
#include <QHash>
#include <QHBoxLayout>
class QTcpServer;
class QTcpSocket;

/** 
 *  @brief T��da reprezentuj�c� server
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

class server2011 : public QMainWindow
{
	Q_OBJECT

public:
	/**
	 * @brief Stavy serveru
	 */
	enum serverState{
		WAITNEWGAME, /**< @brief Stav �ek�n� na zalo�en� hry */
		NEWGAME, /**< @brief Stav �ek�n� na spu�t�n� hry */
		GAMERUN /**< @brief Stav, kdy hra b�� */
	};


	server2011(QWidget *parent = 0, Qt::WFlags flags = 0);
	~server2011();

private:
	Ui::server2011Class ui;

	QHBoxLayout *mainLayout;
	QStringList savedGames;
	QString loginFile;
	int countOfPlayers;
	QTcpServer* server;

	QHash<QTcpSocket*, QString> logins;
	QHash<QString, QString> users;
	QHash<QTcpSocket*, QPair<QString, int> > gamePlayers;
	QHash<QString, int> log;
	QTcpSocket* creatorSocket;
	int port;
	int status;
	QHostAddress addr;

	void login(QTcpSocket* socket, const QString& user, const QString& pass);
	void newGame(QTcpSocket* socket, QString user);
	void joinGame(QTcpSocket* socket, QString user, int position);
	void leaveGame(QTcpSocket* socket, QString user);
	void logOut(QTcpSocket* socket);
	void jumpToPiece(QTcpSocket* socket, QString user, QByteArray message);
	void sendGameList(QTcpSocket* socket, QString user);
	void findSavedGames(QStringList &savedGames, QString user);
	void startGame(QTcpSocket* socket, QList<QByteArray> fileLines);
	void saveGame(QTcpSocket* socket, QString user, QList<QByteArray> fileLines, QString fileName);
	void loadGame(QTcpSocket* socket, QString user, QString fileName);
	void regUser(QTcpSocket* socket, QString user, QString pass);
	void cancelGame();
	void endGame();
	void checkGame(QTcpSocket* socket);
	void sendToAllPlayers(QList <QTcpSocket*> allSockets, QByteArray message);
	void createPlayerList(QByteArray &message);
	void setStatus(int status);
	int getStatus();
	void socketWrite(QTcpSocket* socket, QByteArray data);

public slots:
	void on_pushStartServer_clicked();
	void purgeLog();
	void handleNewConnection();
	void reply();
	void gotDisconnected();
};



#endif // SERVER2011_H
