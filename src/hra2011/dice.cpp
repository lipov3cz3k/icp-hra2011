/** @file dice.cpp
 *  @brief Modul pro pr�ci s hrac� kostkou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "dice.h"

Dice::Dice(QWidget *parent, QString imageDir)
	: QWidget(parent)
{
	pen = new QPen();
	brush = new QBrush(Qt::transparent);
	valueImg = NULL;
	this->imageDir = imageDir;
	this->value = 1;
	this->setMinimumHeight(75);
	this->setMinimumWidth(75);
}

Dice::~Dice()
{

}

int Dice::throwDice(){
	this->value = qrand()%6 + 1;
	Dice::setValue(this->value);
	this->update();
	return this->value;
}

int Dice::getActualValue(){
	return this->value;
}

void Dice::paintEvent(QPaintEvent *event){
	QPainter painter(this);

	painter.setBrush(*brush);
	painter.setPen(*pen);
	painter.setCompositionMode(QPainter::CompositionMode_SourceAtop);
	QRect rect = QRect(1,1, 70, 70);
	
	painter.setRenderHint(QPainter::Antialiasing, true);
	if(valueImg != NULL){
		painter.drawEllipse(rect);
		painter.drawPixmap(rect, *valueImg);
	}
	
}

void Dice::setValue(int value){
	this->value = value;
	if(this->value >0){
		this->valueImg = new QPixmap(this->imageDir+"/croft_"+QString::number(this->value)+".png");
	}
}

void Dice::setActualValue(int value){
	setValue(value);
}