/** @file settings.cpp
 *  @brief Modul s nastaven�m instance hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "settings.h"

Settings::Settings(QWidget *parent, GameSettings *gameSettings)
	: QDialog(parent)
{
	ui.setupUi(this);
	
	this->gameSettings = gameSettings;
	connect(ui.loadMap, SIGNAL(clicked()), this, SLOT(loadMapSlot()));
	ui.mapFilePath->setText(this->gameSettings->getDefaultGameboardsDir()+"/standard_4.xml");

	QStringList colorNames;
	colorNames << "green" << "red" << "blue" << "yellow";

	

	playerNumSelect = 4;
	for(int i = 0; i<playerNumSelect; i++){
		playerNameEdit.append(new QLineEdit("Hrac "+QString(i+'1'), this));
		playerColorCombo.append(new QComboBox(this));
		playerTypeCombo.append(new QComboBox(this));
		computerModeCombo.append(new QComboBox(this));

		playerTypeCombo.last()->addItem(tr("None"));
		playerTypeCombo.last()->addItem(tr("Human"));
		playerTypeCombo.last()->addItem(tr("Computer"));

		computerModeCombo.last()->addItem(tr("Random"));
		computerModeCombo.last()->addItem(tr("Largest move"));
		computerModeCombo.last()->addItem(tr("Prefer to get home"));
		computerModeCombo.last()->addItem(tr("Prefer to get on"));

		int index = 0;
		foreach (const QString &colorName, colorNames) {
			const QColor color(colorName);
			playerColorCombo.last()->addItem(colorName, color);
			const QModelIndex idx = playerColorCombo.last()->model()->index(index++, 0);
			playerColorCombo.last()->model()->setData(idx, color, Qt::BackgroundColorRole);
			playerColorCombo.last()->setEnabled(false);
		}

		ui.playerBox->addWidget(playerTypeCombo.last(), i, 1);
		ui.playerBox->addWidget(playerColorCombo.last(), i, 2);
		ui.playerBox->addWidget(playerNameEdit.last(), i, 3);
		ui.playerBox->addWidget(computerModeCombo.last(), i, 4);
	}
	// defaultn� nastaven� hr���
	playerTypeCombo.at(0)->setCurrentIndex(1);
	playerTypeCombo.at(1)->setCurrentIndex(2);
}

Settings::~Settings()
{

}

void Settings::accept(){
	// uklid
	for(int i = 0; i < players.size(); i++)
		delete players.at(i);
	players.erase(players.begin(), players.end());

	playerNum = 0;
	this->mapFilePath = this->ui.mapFilePath->text();
	Player::PlayerType playerTypeTemp;
	Player::ComputerMode computerModeTemp;
	QColor *playerColor;
	for(int i = 0; i<playerNumSelect; i++){
		switch(this->playerTypeCombo[i]->currentIndex()){
			case 0:
				playerTypeTemp = (Player::PlayerType) 0;
			break;
			case 1:
				playerTypeTemp = Player::HUMAN;
			break;
			case 2:
				playerTypeTemp = Player::COMPUTER;
			break;
			case 3:
				playerTypeTemp = Player::LAN;
			break;
		};

		if(this->playerTypeCombo[i]->currentIndex() > 0){
			int r = ((i+1) * 128)%255;
			int g = ((i+1) * 32)%255;
			int b = ((i+1) * 64)%255;

			playerColor = new QColor(r, g, b);
			if(this->playerTypeCombo[i]->currentIndex() != 2)
				Settings::addPlayer(this->playerNameEdit[i]->text(), playerTypeTemp, playerColor, i+1);
			else
				Settings::addPlayer(this->playerNameEdit[i]->text(), playerTypeTemp, playerColor, i+1, this->computerModeCombo[i]->currentIndex());
			playerNum++;
		}
	}
	if(playerNum >1){
		QFile map(this->mapFilePath);
		if(map.exists()){
			QDialog::accept();
		}else
			 QMessageBox::warning(this, tr("New game error"),  tr("Map file does not exists."));// soubor s mapou neexistuje
	}
	else
		 QMessageBox::warning(this, tr("New game error"),  tr("Too few players. Set at least two players."));// soubor s mapou neexistuje//TODO m�lo hr���;

}

void Settings::loadMapSlot(){
	ui.mapFilePath->setText(QFileDialog::getOpenFileName(this, tr("Select map"),  QDir::currentPath()+"\\"+this->gameSettings->getDefaultGameboardsDir()+"\\", tr("Map files (*.xml)")));
}

PlayerSettings* Settings::getPlayerSetting(int i){
	return this->players.at(i);
}

void Settings::addPlayer(QString playerName, int playerType, QColor *playerColor, int boardPosition, int computerMode){
	this->players.append(new PlayerSettings(this, playerName, (Player::PlayerType) playerType, playerColor, boardPosition, computerMode));
}

void Settings::loadSettings(int numOfPlayers, QString mapFile,int onMove){
	this->playerNum = numOfPlayers;
	this->mapFilePath = mapFile;
	this->onMove = onMove;
}

void Settings::loadCroftValues(QString croftValues){
	this->croftValues = croftValues;
}