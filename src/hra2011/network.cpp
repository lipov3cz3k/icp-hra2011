/** @file network.cpp
 *  @brief Modul pro komunikaci se serverem
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "network.h"

Network::Network(QObject *parent, QString serverAddress, int serverPort)
	: QObject(parent)
{
	
	this->serverAddress = QHostAddress(serverAddress);
	this->serverPort = serverPort;

	if(!this->serverAddress.setAddress(serverAddress))
		return;

	socket = new QTcpSocket(this);
	connect(socket, SIGNAL(connected()), SLOT(gotConnected()));
	connect(socket, SIGNAL(disconnected()), SLOT(gotDisconnected()));
	connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(gotError(QAbstractSocket::SocketError)));
	connect(socket, SIGNAL(readyRead()), SLOT(handleReply()));

	connect(socket, SIGNAL(connected()), parent, SIGNAL(connected()));
	connect(this, SIGNAL(registered()), parent, SIGNAL(registered()));
	connect(this, SIGNAL(notreged()), parent, SIGNAL(notreged()));
	connect(this, SIGNAL(logged()), parent, SIGNAL(logged()));
	connect(this, SIGNAL(wronglogin()), parent, SIGNAL(wronglogin()));

	socket->connectToHost(serverAddress, this->serverPort);
}

Network::~Network()
{

}

void Network::gotConnected(){
	// p�ipojeno
}
void Network::gotDisconnected(){
	// odpojeno - po�le sign�l rodi�ovi o odpojen�
}
void Network::gotError(QAbstractSocket::SocketError error){
}
void Network::handleReply(){
// p��choz� data
	QByteArray rawdata = socket->readAll();
	// argumenty p��kazu
	QList<QByteArray> data = rawdata.split(' ');
	// samotn� p��kaz
	QByteArray command = data.takeFirst();

	qDebug() << "handling data:" << rawdata;

	if(command == "LOGGED"){
	// �sp�n� p�ihl�en�
		emit logged();
	}else if(command == "ALREADYLOGGED"){
	// server ��k�, �e dan� u�ivatel je ji� p�ipojen
		socket->disconnectFromHost();
	} else if(command == "WRONGLOGIN"){
	// server informuje o ne�sp�n�m p�ihl�en� na z�klad� �patn�ch �daj�
	// na chv�li odpoj�me informov�n� o chyb�ch � jde o (na prvn� pohled �sp�n�)
	// pokus o eliminaci dvou chybov�ch dialog� na sob�
/*
		socket->disconnect(SIGNAL(error(QAbstractSocket::SocketError)));
		// zobraz�me chybov� dialog
		QMessageBox::critical(this, tr("Wrong login"),
				      tr("You've entered a wrong username or password!") + "\n" +
				      tr("Connection has been closed by the remote server."));
		// a po zav�en� dialogu sign�l op�t p�ipoj�me
		connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
				SLOT(gotError(QAbstractSocket::SocketError)));
				*/
		emit wronglogin();
	}else if(command == "REGISTERED"){
		emit registered();
	}else if(command == "NOTREGED"){
		emit notreged();
	}else if(command == "LISTING"){
		QList <QByteArray> gameListBA = rawdata.split('\n');
		gameListBA.removeFirst();
		QList <QByteArray>::iterator i;
		QStringList gameList;
		for (i = gameListBA.begin(); i != gameListBA.end(); ++i){
			gameList.append(*i);
		}
		emit gameListReceived(gameList);
		//for (QList<QByteArray>::iterator i = gameList.begin(); i != gameList.end(); ++i)
	}else if(command == "GLEMPTY"){
		QStringList gameList;
		emit gameListReceived(gameList);
	}else if(command == "SAVED"){

	}else if(command == "PLAYERLIST"){
		QList<QByteArray> playerListBA = rawdata.split('\n');
		playerListBA.removeFirst();
		QList <QByteArray>::iterator i;
		QString line;
		QStringList lineArgs;
		QList <QPair<QString, int> > playerList;
		for (i = playerListBA.begin(); i != playerListBA.end(); ++i){
			line = *i;
			lineArgs = line.split(' ');
			playerList.append(qMakePair(lineArgs.at(0), lineArgs.at(1).toInt()));
		}
		emit playerListReceived(playerList);
	}else if(command == "CHECKPLAYERS"){
		QList<QByteArray> playerListBA = rawdata.split('\n');
		playerListBA.removeFirst();
		playerListBA.removeFirst();
		QList <QByteArray>::iterator i;
		QString line;
		QStringList lineArgs;
		QList <QPair<QString, int> > playerList;
		for (i = playerListBA.begin(); i != playerListBA.end(); ++i){
			line = *i;
			lineArgs = line.split(' ');
			playerList.append(qMakePair(lineArgs.at(0), lineArgs.at(1).toInt()));
		}
		emit checkNewGame(false);
		emit playerListReceived(playerList);
	}else if(command == "WAITNEWGAME"){
		emit checkNewGame(true);
	}else if(command == "GAMESTARTED"){
		QList<QByteArray> lines = rawdata.split('\n');
		lines.removeFirst();
		QString xmlFile;
		QList<QByteArray>::iterator i;
		for (i = lines.begin(); i != lines.end(); ++i){
			xmlFile += *i;
		}
		emit gameStarted(xmlFile);
	}else if(command == "GAMELOADED"){
		QList<QByteArray> lines = rawdata.split('\n');
		lines.removeFirst();
		QString xmlFile;
		QList<QByteArray>::iterator i;
		for (i = lines.begin(); i != lines.end(); ++i){
			xmlFile += *i;
		}
		emit gameLoaded(xmlFile);
	}else if(command == "JUMP"){
		int onMove = data.takeFirst().toInt();
		int value = data.takeFirst().toInt();
		int lastPiece = data.takeFirst().toInt();	
		emit jumped(onMove, value, lastPiece);
	}else if(command == "CANNOTJOIN"){
		//QMessageBox::critical(this, tr("Cannot join"), tr("Game is full or game is'nt created") );
	}else{
		/*
		QMessageBox::critical(this, tr("Unknown command from the server"),
				      tr("Server has sent an unknown command which could mean "
					 "that your client version is outdated."));
					 */
	}

}
void Network::socketWrite(QByteArray data){
	socket->write(data);
	socket->flush();
}

void Network::userLogin(QString name, QString password){
		QByteArray request;
		request += QString("LOGIN %1 %2").arg(name).arg(password);
		this->socketWrite(request);
}

bool Network::isLoggedIn(){
	return false;
}

void Network::jump(int onMove, int value, int pieceNumber){
	QByteArray request;
	request += "JUMP " + QString::number(onMove) + " " + QString::number(value) + " " + QString::number(pieceNumber);
	this->socketWrite(request);
}

void Network::userRegister(QString name, QString password){
	QByteArray request;
	request += QString("REGISTER %1 %2").arg(name).arg(password);
	this->socketWrite(request);
}

void Network::getGameList(){
	QByteArray request;
	request += "GAMELIST";	
	this->socketWrite(request);
}

void Network::newNetGame(int numOfPlayers){
	QByteArray request;
	request += tr("NEWGAME %1").arg(numOfPlayers);	
	this->socketWrite(request);
}

void Network::joinGame(int position){
	QByteArray request;
	request += tr("JOINGAME %1").arg(position);	
	this->socketWrite(request);
}

void Network::leaveGame(){
	QByteArray request;
	request += "LEAVE";	
	this->socketWrite(request);
}

void Network::checkCanCreate(){
	QByteArray request;
	request += "CHECKGAME";	
	this->socketWrite(request);
}

void Network::startGame(QString xmlFile){
	QByteArray request;
	request += "STARTGAME \n";
	request += xmlFile;
	this->socketWrite(request);
}

void Network::saveGame(QString fileName, QString xmlFile){
	QByteArray request;
	request += "SAVEGAME \n";
	request += xmlFile;
	this->socketWrite(request);	
}

void Network::loadGame(QString fileName){
	QByteArray request;
	request += "LOADGAME " + fileName + "\n";
}

void Network::logOut(){
	QByteArray request;
	request += "LOGOUT";
	this->socketWrite(request);
}