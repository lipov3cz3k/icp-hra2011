/** @file piece.h
 *  @brief Modul pro pr�ci s figurkou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef PIECE_H
#define PIECE_H

#include <QObject>
//#include "croft.h"
#include "path.h"

/** 
 *  @brief T��da reprezentuj�c� figurku
 *  @author Tom� Lipovsk�
 *  @version 0.3
 *  @date 2011-05-05
 */
class Piece : public QObject
{
	Q_OBJECT

public:
	/**
	 * @brief Vytvo�� figurku
	 * @param parent Rodi�ovsk� QObject
	 * @param playerPath Cesta, po kter� se figurka posouv�
	 * @param playerNumber ��slo hr��e
	 * @param startPosition Po��ta�n� pozice figurky
	 * @param playerColor Barva hr��e
	 * @param playerAvatar Ikonka hr��e
	 */
	Piece(QObject *parent, Path* playerPath, const int playerNumber, const int startPosition, QColor *playerColor, QPixmap *playerAvatar);

	/**
	 * @brief Destruktor figurky
	 */
	~Piece();
	
	/**
	* @brief Zv�razn� dal�� mo�n� skok figurky
	* 
	* P�ed pou�it�m je nutn� nastavit dal�� skok metodou setNextJump
	* @param type Zapne zv�razn�n� pokud je true, jinak zv�razn�n� vypne
	* @param *playerColor Barva hra�e, podle kter�ho se poli�ko zv�razn�
	* @see setNextJump
	*/
	void highLightNext(bool type, QColor *playerColor);

	/**
	* @brief Nastav� figurce dal�� mo�n� skok a zv�razn� ho
	*
	* Pokud je hodnota parametru value -1, tak zru�� zv�razn�n� a nastav� dal�� skok na hodnotu -1
	* @param value Hodnota hledan�ho dal�iho skoku
	* @see highLightNext
	*/
	void setNextJump(int value);
	
	/**
	 * @brief Vr�t� true, pokud je pol��ko dal��ho skoku dome�ek
	 *
	 * Pol��ko dal��ho skoku lze nastavit metodou setNextJump
	 * @return Vr�t� true, pokud je dal�� pol��ko dome�ek, jinak false
	 * @see setNextJump
	 */
	bool isNextHome();

	/**
	 * @brief Vr�t� true, pokud figurka stoj� na dome�ku
	 * @return Vr�t� true, pokud je aktu�ln� pol��ko dome�ek, jinak false
	 */
	bool isHome();

	/**
	 * @brief Vr�t� true, pokud figurka stoj� na pol��ku OUT
	 * @return Vr�t� true, pokud je aktu�ln� pol��ko OUT, jinak false
	 */
	bool isOut();

	/**
	 * @brief Vr�t� metriku dal��ho skoku (��m v�t��, t�m lep��)
	 * @return Metrika skoku
	 */
	int getNextMatric();

	/**
	 * @brief Sko�� s figurkou na dal�� mo�nou pozici
	 *
	 * Pol��ko dal��ho skoku lze nastavit metodou setNextJump
	 * @return Vr�t� true, pokud se skok povedl
	 */
	bool jumpToNextValue();

	/**
	 * @brief Vr�t� ��st XML s nastaven�m figurky
	 * @return Nastaven� figurky
	 * @see Player::saveGamePlayer
	 */
	QString saveGamePiece();

	/**
	* @brief Presune figurku na novou pozici.
	* 
	* Pokud na nov� pozici ji� figurka je, tak ji vyhod� do OUT
	* @param position ��slo pozice (z hlediska hr��ovi cesty), kam se figurka p�esune
	* @return Vr�t� true, pokud se p�esun povedl (dal�� pozice existuje)
	*/
	bool jumpAt(int position);

public slots:
	/**
	* @brief Pokud existuje dal�� mo�n� pozice, tak na ni p�esune figurku a zru�� v�echna zv�razn�n�.
	*
	* Dal�� pozice skoku lze nastavit metodou setNextJump
	* @see setNextJump
	*/
	void croftClicked();

	/**
	 * @brief Vyhod� figurku do jej� startovn� pozice (OUT)
	 */
	void pieceOut();

signals:

	/**
	 * @brief Zablokuje ostatn� hr��ovy figurky
	 */
	void disableOtherPieces();
	
	/**
	 * @brief Hr�no s figurkou
	 * @param pieceNumber ��slo figurky
	 */
	void playedWith(int pieceNumber);

private:
	Path *playerPath;
	int startPosition;
	int actualPosition;
	int playerNumber;
	int nextPosition;

	QColor *playerColor;
	QPixmap *playerAvatar;
};

#endif // PIECE_H
