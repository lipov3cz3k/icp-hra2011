/** @file gameboard.cpp
 *  @brief Modul pro pr�ci s hern� deskou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "gameboard.h"

GameBoard::GameBoard(QWidget *parent, QString &mapFile, int players, const QString &imageDir)
	: QWidget(parent)
{
	this->players = players;
	gameBoardLayout = new QGridLayout(this);	
	gameBoardLayout->setSizeConstraint(QLayout::SetFixedSize);
	gameBoardLayout->setSpacing(0);
	
	this->imageDir = imageDir;
	for (int i = 0; i < 6; i++){
        numOfOccur[i] = 0;
    }

	/* Herni plan nacteny ze souboru */
	pathLogical = new Path(this);
	if(!GameBoard::loadGamePlane(mapFile)){
		// spatne nactena a vytvorena mapa
		;
	}
	
	/* Automaticky generovany plan
	GameBoard::renderCross(5);
	*/
	
}

GameBoard::~GameBoard()
{
	delete map2D;
	//delete this->backgroundImg;
}

bool GameBoard::loadGamePlane(const QString &filePath){
	pathSize = 0;
	Croft *tempCroft;

	Position previous;
	Position actual;
	Position finish;

	QFile mapFile(filePath);
     if (!mapFile.open(QIODevice::ReadOnly | QIODevice::Text))
         return false;

	QXmlStreamReader xmlStream(&mapFile);
	QString map;
	while(!xmlStream.atEnd())
    {
        xmlStream.readNext();
        if(xmlStream.isStartElement())
        {
			if(xmlStream.name() == "map"){
				map_size_x = xmlStream.attributes().value("size_x").toString().toInt();
				map_size_y = xmlStream.attributes().value("size_y").toString().toInt();
				map_players = xmlStream.attributes().value("maxPlayers").toString().toInt();
				map = xmlStream.readElementText().trimmed();
			}else if(xmlStream.name() == "previous"){
				previous.set(xmlStream.attributes().value("x").toString().toInt(), xmlStream.attributes().value("y").toString().toInt());
			}else if(xmlStream.name() == "start"){
				actual.set(xmlStream.attributes().value("x").toString().toInt(), xmlStream.attributes().value("y").toString().toInt());
			}else if(xmlStream.name() == "finish"){
				finish.set(xmlStream.attributes().value("x").toString().toInt(), xmlStream.attributes().value("y").toString().toInt());
			}
        }
    }
	mapFile.close();
	if(this->map_players < this->players)
		return false;

	// vytvori 2D mapu ze stringu
	map2D = new QVector<QVector<char> > (map_size_x, QVector<char>(map_size_y));
	QString line;
	char point;
	for(int i = 0; i<map_size_x; i++){
		line = map.section('\n', i, i).trimmed();
			for(int j=0; j<map_size_y; j++){
				point = line[j].toAscii();
				(*map2D)[i][j] = point;
				
				if(point == '.'){
					tempCroft = NULL;
				}else if(point == 'X'){
					// standardni policko
					tempCroft = new Croft(this, Croft::NORMAL, i, j);
					pathSize++;
				}else if(point >= 'a' && point <= 'j'){
					// nasazovaci pozice hrace 'point'-
					tempCroft = new Croft(this, Croft::START, i, j);
					tempCroft->setOwner(point-'a'+1);
					pathSize++;
				}else if((point >= '0' && point <= '9') || point == 'I'){
					// domecek hrace 'point'-
					tempCroft = new Croft(this, Croft::HOME, i, j);
					pathSize++;
				}else if(point >= 'A' && point <= 'J'){
					// pozice OUT hrace 'point'-
					tempCroft = new Croft(this, Croft::OUT, i, j);
					tempCroft->setOwner(point-'A'+1);
				}else if(point == 'T'){
					// odbocka do domecku
					tempCroft = new Croft(this, Croft::NORMAL, i, j);
					pathSize++;
				}		
				crofts.append(tempCroft);
			}
	}
	pathSize -= (4-players)*4;

	// vytvori serazenou cestu
	makeLogicalPath(finish, previous, actual);


	// umisti vsechny policka
	for(QList<Croft *>::iterator i = crofts.begin(); i!=crofts.end(); i++){
		if(*i != NULL)
			gameBoardLayout->addWidget(*i, (*i)->getPositionX(), (*i)->getPositionY());
	}	
	return true;
}

void GameBoard::makeLogicalPath(Position finish, Position previous, Position actual){
	Croft *temp = crofts.at(actual.getOffset(map_size_y));
	temp->setValue( GameBoard::generateRandomNumber(), this->imageDir);
 	if(finish != actual){
		pathLogical->append(crofts.at(actual.getOffset(map_size_y)));
		GameBoard::makeLogicalPath(finish, actual, GameBoard::findNextCroft(Croft::NORMAL, previous, actual));
	}else{
		pathLogical->append(crofts.at(actual.getOffset(map_size_y)));
		return;
	}
}

Position GameBoard::findNextCroft(int type, Position previous, Position actual){
	Position next(0,0);
	Position left = actual.left();
	Position right = actual.right();
	Position up = actual.up();
	Position down = actual.down();
	if(right != previous && right.getY() < map_size_y && GameBoard::isCroft(type, (*map2D)[right.getX()][right.getY()])){
			next = right;
	}else if(left != previous && left.getY() >= 0  && GameBoard::isCroft(type, (*map2D)[left.getX()][left.getY()])){
			next = left;
	}else if(down != previous && down.getX() < map_size_x  && GameBoard::isCroft(type, (*map2D)[down.getX()][down.getY()])){
			next = down;
	}else if(up != previous && up.getX() >= 0 && GameBoard::isCroft(type, (*map2D)[up.getX()][up.getY()])){
			next = up;
	}
	return next;
}

bool GameBoard::isCroft(int type, char character){
	if(type == Croft::NORMAL){
		if(character == 'X' || character =='T' || (character >= 'a' && character <='j'))
			return true;
	}else if(type == Croft::HOME){
		if(character == 'I' || character=='T')
			return true;
	}
	return false;
}


Path* GameBoard::makePlayerPath(QObject *parent, int player){
	Path *result = new Path(parent);
	char point = '.';
	char playerChar = player+'0';
	int x = -1;
	int y = -1;
	while(point != playerChar && y < map_size_y){
		y++;
		y = y%map_size_y;
		if(y == 0)
			x++;
		point = (*map2D)[x][y];		
	}

	// v promennych x a y je pozice posledniho domecku
	Position home(x, y);
	Position previous(-1, -1);
	Position positionTemp;
	Croft *croftTemp;
	for(int i = 0; i<4; i++){
		croftTemp = crofts.at(home.getOffset(map_size_y));
		result->prepend(croftTemp);		
		croftTemp->setValue(GameBoard::generateRandomNumber(), this->imageDir);
		croftTemp->setOwner(player);
		positionTemp = GameBoard::findNextCroft(Croft::HOME, previous, home);
		previous = home;
		home = positionTemp;
	}
		// v home je pozice odbocky
	int from = pathLogical->getCroftOffset(home.getX(), home.getY());
	int croftType;
	do{
		croftTemp = pathLogical->at(from);
		croftType = croftTemp->getType();
		result->prepend(croftTemp);
		from--;
		if(from<0)
			from = pathLogical->size()-1;

	}while(croftType != Croft::START || croftTemp->getOwner() != player);
		// v croftTemp je nasazovac� policko daneho hrace

	playerChar = player + 'A' - 1;
	x = -1;
	y = -1;
	int out = 0;
	int croftsOffset = -1;
	while(out != 4 && y < map_size_y){
		croftsOffset++;
		y++;
		y = y%map_size_y;
		if(y == 0)
			x++;
		if((*map2D)[x][y] == playerChar){
			result->prepend(crofts.at(croftsOffset));
			out++;
		}
	}
	return result;
}

void GameBoard::renderCross(int edge){
	int thinEdge = (edge/2)-1;
	pathSize = 12*edge-4*thinEdge;

	int pos=0;
	// vygeneruje cestu
	for(int i=0; i<pathSize; i++ ){
		crofts.append(new Croft(this, Croft::NORMAL, pos, pos));
	}
	// vygeneruje cilove domecky
	for(int i=0; i<this->players*4; i++ ){
		crofts.append(new Croft(this, Croft::HOME, pos, pos));
	}
	// vygeneruje startovaci pozice
	for(int i=0; i<this->players*4; i++){
		crofts.append(new Croft(this, Croft::OUT, pos, pos));
	}
	// nastavi startovni policka
	for(int i=0; i<this->players; i++ ){
		crofts.at(i*(3*edge-thinEdge))->setType(Croft::START);	
	}

	int pos_y = edge;
	int pos_x = 0;
	
	// umisti cestu na hraci pole
	for(int i=0; i<pathSize; i++){
		gameBoardLayout->addWidget(crofts.at(i), pos_y, pos_x);
		if(i<edge*3-thinEdge){
			//1. kvadrant
			if(i>=edge && i<edge*2)
				pos_y--;
			else
				pos_x++;
		}else if(i<edge*6-2*thinEdge){
			//2. kvadrant
			if(i>=edge*4-thinEdge && i<edge*5-thinEdge)
				pos_x++;
			else
				pos_y++;
		}else if(i<edge*9-3*thinEdge){
			//3. kvadrant
			if(i>=edge*7-2*thinEdge && i<edge*8-2*thinEdge)
				pos_y++;
			else
				pos_x--;
		}else{
			//4. kvadrant
			if(i>=edge*10-3*thinEdge && i<edge*11-3*thinEdge)
				pos_x--;
			else
				pos_y--;
		}
	}
			
	// umisti startovni pozice a domecky
	pos_x=0;
	pos_y=0;
	for(int i=0; i<this->players*4; i++){
		if(i<4){
			//1. hrac - start
			pos_x = i%4;
			pos_y = 0;
			gameBoardLayout->addWidget(crofts.at(i+pathSize+4*this->players), pos_y, pos_x);
			// cil
			pos_x = 1 + i%4; 
			pos_y = edge + thinEdge;
			gameBoardLayout->addWidget(crofts.at(i+pathSize), pos_y, pos_x);
		}else if(i<8){
			//2. hrac - start
			pos_x = 3*edge-thinEdge;
			pos_y = i%4;
			gameBoardLayout->addWidget(crofts.at(i+pathSize+4*this->players), pos_y, pos_x);
			// cil
			pos_x = edge + thinEdge; 
			pos_y = 1 + i%4;
			gameBoardLayout->addWidget(crofts.at(i+pathSize), pos_y, pos_x);
		}else if(i<12){
			//3.hrac - start
			pos_x = 3*edge - i%4 -thinEdge;
			pos_y = 3*edge - thinEdge;
			gameBoardLayout->addWidget(crofts.at(i+pathSize+4*this->players), pos_y, pos_x);
			// cil
			pos_x = 3*edge-thinEdge - 1 - i%4; 
			pos_y = edge + thinEdge;
			gameBoardLayout->addWidget(crofts.at(i+pathSize), pos_y, pos_x);
		}else if(i<16){
			//4.hrac - start
			pos_x = 0;
			pos_y = 3*edge - i%this->players - thinEdge;
			gameBoardLayout->addWidget(crofts.at(i+pathSize+4*this->players), pos_y, pos_x);
			// domecek
			pos_x = edge + thinEdge; 
			pos_y = 3 * edge - thinEdge - 1 - i%4;
			gameBoardLayout->addWidget(crofts.at(i+pathSize), pos_y, pos_x);
		}
	}
}

int GameBoard::generateRandomNumber(){
    int randInt;
    do {
    randInt = qrand()%6 + 1;
    } while (numOfOccur[randInt-1] == ((pathSize-1)/6)+1);
    numOfOccur[randInt-1]++;
    return randInt;
}

QString GameBoard::saveGameGameboard(){
	QString result;
	result = "\t<gameboard>\n";
	for(QList<Croft *>::iterator i = crofts.begin(); i!=crofts.end(); i++){
		if(*i != NULL)
			result += (*i)->getValue()+'0';
		else
			result += "x";
	}
	result += "\n\t</gameboard>\n";
	return result;
}

void GameBoard::loadCroftValues(QString croftValues){
	int size = map_size_x * map_size_y;
	char point;
	for(int i = 0; i<size; i++){
		point = croftValues.at(i).toAscii();
		if(point > '0' && point <= '6'){
			crofts.at(i)->setValue(point-'0', this->imageDir);
		}
	}
}

Position::Position(){
	Position::set(0, 0);
}

Position::Position(int x, int y){
	Position::set(x, y);
}

Position &Position::operator= (const Position & other){
	x = other.x;
	y = other.y;
	return *this;
}

bool Position::operator ==(const Position &other) const{
	return this->x == other.x && this->y == other.y;
}

bool Position::operator !=(const Position &other) const{
	return this->x != other.x || this->y != other.y;
}

int Position::getX(){
	return this->x;
}

int Position::getY(){
	return this->y;
}

int Position::getOffset(int width){
	return this->x*width+this->y;
}

void Position::set(int x, int y){
	this->x = x;
	this->y = y;
}

Position Position::right(){
	return Position(this->x, this->y+1); 
}

Position Position::left(){
	return Position(this->x, this->y-1); 
}

Position Position::up(){
	return Position(this->x-1, this->y); 
}

Position Position::down(){
	return Position(this->x+1, this->y); 
}
