/** @file path.h
 *  @brief Modul pro pr�ci s cestou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef PATH_H
#define PATH_H

#include <QObject>
#include "croft.h"

/** 
 *  @brief T��da reprezentuj�c� cestu na hern� desce
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 *
 *  Cesta se skl�d� z hern�ch pol��ek
 *  @see Croft
 */
class Path : public QObject
{
	Q_OBJECT

public:
	/**
	 * @brief Vytvo�� cestu
	 * @param parent Rodi�ovsk� QObject
	 */
	Path(QObject *parent);
	
	/**
	 * @brief Destruktor cesty
	 */
	~Path();

	/**
	 * @brief P�id� na konec cesty hern� pol��ko
	 * @param croft Hern� pol��ko
	 */
	void append(Croft* croft);

	/**
	 * @brief P�id� na za��tek cesty hern� pol��ko
	 * @param croft Hern� pol��ko
	 */
	void prepend(Croft* croft);

	/**
	 * @brief Vr�t� d�lku cesty
	 * @return d�lka cesty
	 */
	int size();

	/**
	 * @brief Vr�t� pol��ko na dan� pozici
	 * @param position Posice hledan�ho pol��ka
	 * @return Hern� pol��ko
	 */
	Croft* at(int position);

	/**
	 * @brief Zjist� pozici dal��ho skoku
	 * @param actualPosition Aktu�ln� pol��ko
	 * @param value Hodnota dal��ho pol��ka
	 * @return Pozice dal��ho pol��ka
	 */
	int getNextJump(int actualPosition, int value);

	/**
	 * @brief Vr�t� pozici v cest� na z�klad� sou�adnic
	 * @param x ��slo ��dku
	 * @param y ��slo sloupce
	 * @return Pozice v cest�
	 */
	int getCroftOffset(int x, int y);


private:
	int pathSize;
	QList<Croft*> path;	
};

#endif // PATH_H
