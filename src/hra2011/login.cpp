/** @file login.h
 *  @brief Modul pro p�ihla�ov�n� u�ivatel� na server
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "login.h"


Login::Login(QWidget *parent, GameSettings *gamesettings)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.registerButton->setEnabled(false);
	ui.nameEdit->setEnabled(false);
	ui.passwordEdit->setEnabled(false);
	ui.loginButton->setEnabled(false);
	
	this->parent = parent;
	this->serverAddress = gamesettings->getServerAddress();
	this->serverPort = gamesettings->getServerPort();

	connect(ui.connectButton, SIGNAL(clicked()), this, SLOT(connectServer()));
	connect(ui.registerButton, SIGNAL(clicked()), this, SLOT(userRegister()));
	connect(ui.loginButton, SIGNAL(clicked()), this, SLOT(userLogIn()));
	
}

Login::~Login()
{

}

void Login::connectServer(){
	ui.connectButton->setEnabled(false);
	ui.connectButton->setText("Connecting");

	network = new Network(parent, this->serverAddress, this->serverPort);
}


void Login::gotConnected(){
	ui.connectButton->setText("Connected");

	ui.registerButton->setEnabled(true);
	ui.nameEdit->setEnabled(true);
	ui.passwordEdit->setEnabled(true);
	ui.loginButton->setEnabled(true);
}

void Login::userRegister(){
	QString name = ui.nameEdit->text();
	QString password = ui.passwordEdit->text();

	ui.registerButton->setEnabled(false);
	network->userRegister(name, password);
}

void Login::registeredSlot(){
 QMessageBox::information(this, tr("Register successful"),  tr("You have been successfuly registered."));
 ui.registerButton->setEnabled(true);
}

void Login::notregedSlot(){
 ui.registerButton->setEnabled(true);
 QMessageBox::warning(this, tr("Register error"),  tr("Could not register. Same username is allready used."));
}
void Login::userLogIn(){
	QString name = ui.nameEdit->text();
	QString password = ui.passwordEdit->text();

	ui.loginButton->setEnabled(false);
	ui.loginButton->setText("Logging in");
	network->userLogin(name, password);
}
void Login::loggedSlot(){
	ui.loginButton->setText("OK");
	QDialog::accept();
}
void Login::wrongLoginSlot(){
	ui.loginButton->setEnabled(true);
	ui.loginButton->setText("Log in");
	QMessageBox::warning(this, tr("Wrong login"),  tr("Wrong name or password"));
}

Network* Login::getNetwork(){
	return this->network;
}