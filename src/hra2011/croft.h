/** @file croft.h
 *  @brief Modul pro pr�ci s hern�m pol��kem
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef CROFT_H
#define CROFT_H

#include <QWidget>

/** 
 *  @brief T��da reprezentuj�c� hern� pol��ko
 *  @author Tom� Lipovsk�
 *  @version 0.3
 *  @date 2011-05-05
 */
class Croft : public QWidget
{
	Q_OBJECT

public:
	/**
	 * @brief Typ hern�ho pol��ka
	 */
	enum FieldType {
		OUT, /**< @brief St��da�ka */
		HOME, /**< @brief Dome�ek */
		START, /**< @brief Startovn� pol��ko */
		NORMAL /**< @brief Norm�ln� hern� pol��ko */
	};

	/**
	 * @brief Vytvo�� nov� hern� pol��ko
	 * @param parent Rodi�ovsk� QWidget
	 * @param type Typ hern�ho pol��ka z v��tu FieldType
	 * @param pos_x Pozice x (��dek) 
	 * @param pos_y Pozice y (sloupec) 
	 */
	Croft(QWidget *parent, FieldType type, int pos_x, int pos_y);

	/**
	 * @brief Destruktor hern�ho pol��ka 
	 */
	~Croft();

	/**
	 * @brief Nastav� rozm�ry hern�ho pol��ka.
	 */
	QSize sizeHint() const {return QSize (33, 33);}; // velikost policka v gridu

	/**
	 * @brief Nastav� hodnoty pro vykreslen� (barvu, v�pl�, obr�zek)
	 * @param type Typ hern�ho pol��ka z v��tu FieldType
	 * @see FieldType, getType
	 */
	void setType(FieldType type);

	/**
	 * @brief Vr�t� typ pol��ka (podle v��tu FieldType).
	 * @return Typ pol��ka FieldType
	 * @see FieldType, setType
	 */
	int getType();

	/**
	 * @brief Vr�t� hodnotu pol��ka (1-6).
	 * @return Hodnota pol��ka
	 * @see setValue
	 */
	int getValue();

	/**
	 * @brief Nastav� hodnotu pol��ka (1-6).
	 * @param value Hotnota pol��ka
	 * @param imageDir Adres�� s podkladov�m obr�zkem (pojmenovan� jako croft_X.png /X = 1-6/)
	 * @see getValue
	 *
	 */
	void setValue(int value, QString imageDir);

	/**
	 * @brief Nastav� sou�adnice, kde je pol��ko vykresleno.
	 * @param pos_x Pozice x (��dek) 
	 * @param pos_y Pozice y (sloupec)
	 * @see getPositionX, getPositionY
	 */
	void setPosition(int pos_x, int pos_y);

	/**
	 * @brief Vr�t� ��slo ��dku, kde je pol��ko vykresleno
	 * @return ��slo ��dku
	 * @see setPosition, getPositionY
	 */
	int getPositionX();

	/**
	 * @brief Vr�t� ��slo sloupce, kde je pol��ko vykresleno
	 * @return ��slo sloupce
	 * @see setPosition, getPositionX
	 */
	int getPositionY();

	/**
	 * @brief Ulo�� odkaz na figurku, kter� na pol��ku stoj�.
	 *
	 * Vytvo�� sign�ly pro kliknut� na pol��ko
	 * @param *piece Objekt figurky, kter� stoj� na pol��ku
	 * @see isUsedBy
	 */
	void setUsedBy(QObject *piece);

	/**
	 * @brief Postav� na pol��ko figurku.
	 * @param *piece Figurka, kter� bude st�t na pol��ku
	 * @param player ��slo hr��e, krer� vlastn� figurku
	 * @param *playerAvatar Obr�zek hr��e
	 * @see isUsedBy
	 */
	void setUsedBy(QObject *piece, int player, QPixmap *playerAvatar);

	/**
	 * @brief Vr�t� ��slo hr��e, kter�mu pol��ko pat�� (out, start, dome�ek)
	 *
	 * Pou��v� se u pol��ek typu out, start nebo dome�ek
	 * @return ��slo hr��e
	 * @see setOwner
	 */
	int getOwner();

	/**
	 * @brief Nastav� ��slo hr��e, kter�mu pol��ko pat�� 
	 *
	 * Pou��v� se u pol��ek typu out, start nebo dome�ek
	 * @param player ��slo hr��e
	 * @see getOwner
	 */
	void setOwner(int player);

	/**
	 * @brief Nastav� pol��ku barvu
	 *
	 * Pou��v� se u pol��ek typu out, start nebo dome�ek
	 * @param *color Nov� barva
	 * @param playerNumber ��slo hr��e
	 */
	void setOwnerColor(QColor *color, int playerNumber);

	/**
	 * @brief Zv�razn� pol��ko pr�hlednou barvou
	 * @param type Pokud je true, zapne zv�razn�n�, jinak vypne
	 * @param *playerColor Barva hr��e, podle kter�ho se pol��ko zv�razn�
	 */
	void highLight(bool type, QColor *playerColor);

	/**
	 * @brief Vrac� ��slo hr��e, kter� aktu�ln� stoj� na pol��ku
	 * @return ��slo hr��e
	 * @see setUsedBy
	 */
	int isUsedBy();

	/**
	 * @brief Pokud na pol��ku stoj� figurka, tak vyd� sign�l sigPieceOut, kter� figurku vyhod� zp�t do OUT
	 * @see sigPieceOut
	 */
	void pieceOut();

signals:
	/**
	 * @brief Sign�l vyvol� posunut� figurky, na kterou se kleplo my��.
	 */
	void sigClickedOnCroft();

	/**
	 * @brief Sign�l vyvol� vyhozen� figurky do dome�ku.
	 */
	void sigPieceOut();

protected:
	/**
	 * @brief Vykresl� pol��ko.
	 */
	void paintEvent(QPaintEvent *event);

	/**
	 * @brief Pokud na pol��ku stoj� figurka, tak po kliknut� vyd� sign�l sigClickedOnCroft.
	 */
	void mousePressEvent(QMouseEvent *event);

private:
	FieldType type;
	int value;
	QPen *pen;
    QBrush *brush;
	QPixmap *valueImg;
	QPixmap *avatarImg;
	QObject *usedBy;
	int usedByPlayer;
	int owner;
	int pos_x;
	int pos_y;
};

#endif // CROFT_H
