/** @file network.h
 *  @brief Modul pro komunikaci se serverem
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QHostAddress>
#include <QAbstractSocket>

#include <QtNetwork>


/** 
 *  @brief T��da pro s�ovou hru
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.1
 *  @date 2011-05-06
 */
class Network : public QObject
{
	Q_OBJECT

public:
	Network(QObject *parent, QString serverAddress, int serverPort);
	~Network();
	void userLogin(QString name, QString password);
	bool isLoggedIn();
	void userRegister(QString name, QString password);
	void getGameList();
	void newNetGame(int numOfPlayers);
	QStringList getPlayerList();
	void joinGame(int position);
	void saveGame(QString fileName, QString xmlFile);
	void startGame(QString xmlFile);
	void loadGame(QString fileName);
	void leaveGame();
	void logOut();
	void jump(int onMove, int value, int pieceNumber);
	void checkCanCreate();

private:
	QHostAddress serverAddress;
	int serverPort;
	QTcpSocket* socket;
	void socketWrite(QByteArray data);
	
signals:
	void registered();
	void notreged();
	void logged();
	void wronglogin();
	void jumped(int onMove, int value, int lastPiece);
	void gameListReceived(const QStringList& gameList);
	void gameStarted(const QString &xmlFile);
	void gameLoaded(const QString xmlFile);
	void playerListReceived(const QList <QPair<QString, int> > playerList);
	void checkNewGame(const bool &created);

private slots:
	void gotConnected();
	void gotDisconnected();
	void gotError(QAbstractSocket::SocketError error);
	void handleReply();
};

#endif // NETWORK_H
