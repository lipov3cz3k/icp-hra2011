/** @file dice.h
 *  @brief Modul pro pr�ci s hrac� kostkou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef DICE_H
#define DICE_H

#include <QWidget>


/** 
 *  @brief T��da reprezentuj�c� hrac� kostku
 *  @author Tom� Lipovsk�
 *  @version 0.3
 *  @date 2011-05-05
 */
class Dice : public QWidget
{
	Q_OBJECT

public:

	/**
	 * @brief Vytvo�� hrac� kostku
	 * @param parent Rodi�ovsk� QWidget
	 * @param imageDir Slo�ka s obr�zky
	 */
	Dice(QWidget *parent, QString imageDir);

	/**
	 * @brief Destruktor hrac� kostky
	 */
	~Dice();

	/**
	 * @brief Vygeneruje n�hodn� ��slo (1-6)
	 * @return Vygenerovan� hodnota
	 * @see getActualValue
	 */
	int throwDice();

	/**
	 * @brief Vr�t� aktu�ln� vygenerovan� ��slo (1-6)
	 * @return Vygenerovan� hodnota
	 */
	int getActualValue();

	/**
	 * @brief Ru�n� nastav� hodnotu kostky
	 * @param value Hodnota kostky
	 */
	void setActualValue(int value);

protected:
	void paintEvent(QPaintEvent *event);
private:
	int value;
	QPen *pen;
    QBrush *brush;
	QPixmap *valueImg;
	QString imageDir;
	void setValue(int value);
};

#endif // DICE_H
