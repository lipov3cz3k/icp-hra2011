/** @file playersettings.cpp
 *  @brief Modul s nastaven�m hr��e
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "playersettings.h"

PlayerSettings::PlayerSettings(QObject *parent, QString playerName, Player::PlayerType playerType, QColor *playerColor, int boardPosition, int computerMode)
	: QObject(parent)
{
	this->playerName = playerName;
	this->playerType = playerType;
	this->playerColor = playerColor;
	this->boardPosition = boardPosition;
	this->computerMode = computerMode;
}

PlayerSettings::~PlayerSettings()
{

}

QString PlayerSettings::getPlayerName(){
	return this->playerName;
}

Player::PlayerType PlayerSettings::getPlayerType(){
	return this->playerType;
}

int PlayerSettings::getBoardPosition(){
	return this->boardPosition;
}

QColor* PlayerSettings::getPlayerColor(){
	return this->playerColor;
}

Player::ComputerMode PlayerSettings::getComputerMode(){
	return static_cast<Player::ComputerMode>(this->computerMode);
}
