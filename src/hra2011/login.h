/** @file login.h
 *  @brief Modul pro p�ihla�ov�n� u�ivatel� na server
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include "ui_login.h"
#include "network.h"
#include "gamesettings.h"

class Login : public QDialog
{
	Q_OBJECT

public:
	Login(QWidget *parent, GameSettings *gamesettings);
	~Login();
	Network* getNetwork();

public slots:
	void gotConnected();
	void registeredSlot();
	void notregedSlot();
	void loggedSlot();
	void wrongLoginSlot();

private:
	Ui::Login ui;
	QWidget *parent;
	QString serverAddress;
	int serverPort;
	Network *network;

private slots:
	void connectServer();
	void userRegister();
	void userLogIn();
	
};

#endif // LOGIN_H
