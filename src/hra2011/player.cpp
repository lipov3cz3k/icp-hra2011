/** @file player.cpp
 *  @brief Modul pro pr�ci s hr��em
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "player.h"

Player::Player(QObject *parent, GameBoard *board, const int playerNumber, PlayerType playerType, QString imageDir, QColor *color, QString playerName)
	: QObject(parent)
{
	playerPath = new Path(this);
	this->playerNumber = playerNumber;
	this->playerName = playerName;
	this->playerType = playerType;
	this->piecesAtHome = 0;
	

	this->playerColor = new QColor(*color);

	this->playerAvatar = new QPixmap(imageDir+"/player_"+QString::number(playerNumber)+".png");

	// vytvori svoji vlastni cestu
	playerPath = board->makePlayerPath(this, this->playerNumber);
	for(int i = 0; i < playerPath->size(); i++){
		playerPath->at(i)->setOwnerColor(playerColor, playerNumber);
	}

	// vytvori figurky
	for(int i = 0; i < 4; i++){
		pieces.append(new Piece(this, playerPath, this->playerNumber, i, playerColor, this->playerAvatar));
	}
	connect(this, SIGNAL(playerFinished()), parent, SLOT(playerFinishedSlot()));
	connect(this, SIGNAL(finishTurn()), parent, SLOT(nextPlayer()));
	connect(this, SIGNAL(playedWith(int)), parent, SLOT(playedWith(int)));
}

Player::~Player()
{
	delete this->playerAvatar;
	delete this->playerColor;
}

Croft* Player::getCroftAt(int position){
	return playerPath->at(position);
}

int Player::getPlayerNumber(){
	return this->playerNumber;
}

QString Player::getPlayerName(){
	return this->playerName;
}

void Player::setNextJump(int value){
	for(QList<Piece *>::iterator i = pieces.begin(); i!=pieces.end(); i++){
		if(*i != NULL)
			(*i)->setNextJump(value);
	}	
		
	if(value >= 0 && this->playerType == Player::COMPUTER){
		Player::computerMod();
		Player::disableNextJump();
	}
	
}

void Player::disableNextJump(){
	this->piecesAtHome = 0;
	for(QList<Piece *>::iterator i = pieces.begin(); i!=pieces.end(); i++){
		if(*i != NULL)
			(*i)->setNextJump(-1);
		if((*i)->isHome())
			this->piecesAtHome++;
	}	
	if(Player::finished()){
		emit playerFinished();
	}
		emit finishTurn();
}

void Player::computerMod(){
	QList<Piece *>::iterator largest = NULL;
	QList<Piece *>::iterator isOut = NULL;
	QList<Piece *>::iterator isNextHome = NULL;
	int largestValue = -1;
	int temp = 0;
	bool res = false;
	bool nextHome = false;
	bool getOut = false;

	for(QList<Piece *>::iterator i = pieces.begin(); i!=pieces.end(); i++){
		if(*i != NULL){
			if((temp = (*i)->getNextMatric()) >= largestValue){
				largestValue = temp;
				largest = i;
			}
			if((*i)->isOut()){
				isOut = i;
				getOut = true;
			}
			if((*i)->isNextHome()){
				isNextHome = i;
				nextHome = true;
			}
		}
	}

	if(this->computerMode == LONGEST){
		res = (*largest)->jumpToNextValue();
	}else if(this->computerMode == RANDOM){
		res = pieces.at(qrand()%4)->jumpToNextValue();
	}else if(this->computerMode == NEXTHOME && nextHome){
		res = (*isNextHome)->jumpToNextValue();
	}else if(this->computerMode == GETOUT && getOut){
		res = (*isOut)->jumpToNextValue();
	}
	if(!res)
		res = (*largest)->jumpToNextValue();	
}

int Player::getPlayerType(){
	return this->playerType;
}

QString Player::saveGamePlayer(){
	QString result;
	result = "\t<player boardPosition=\""+QString::number(playerNumber)+"\" name=\""+this->playerName+"\" ";
	result += "type=\""+QString::number(this->playerType)+"\" ";
	result += "colorR=\""+QString::number(this->playerColor->red())+"\" colorG=\""+QString::number(this->playerColor->green())+"\" colorB=\""+QString::number(this->playerColor->blue())+"\" ";
	if(this->playerType == COMPUTER)
		result += "computerMode=\""+QString::number(this->computerMode)+"\" ";
	result += ">\n";
	for(QList<Piece *>::iterator i = pieces.begin(); i!=pieces.end(); i++){
		if(*i != NULL)
			result += (*i)->saveGamePiece();
	}	
	result += "\t</player>\n";
	return result;
}

bool Player::finished(){
	if(this->piecesAtHome == 4)
		return true;
	else
		return false;
}

void Player::setComputerMode(ComputerMode mode){
	this->computerMode = mode;
}

void Player::setPieceTo(int piece, int newPosition){
	this->pieces.at(piece)->jumpAt(newPosition);
}

bool Player::jumpNextWithPiece(int piece){
	return this->pieces.at(piece)->jumpToNextValue();
}

void Player::setPlayerType(PlayerType playerType){
	this->playerType = playerType;
}