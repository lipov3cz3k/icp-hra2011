/** @file hra2011.h
 *  @brief Modul hry �lov��e, nezlob se!
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "hra2011.h"

hra2011::hra2011(QApplication *app, QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	// generator nahodnych cisel
	QTime midnight(0, 0, 0);
    qsrand(midnight.secsTo(QTime::currentTime()));

	ui.setupUi(this);
	this->setWindowTitle(QString(tr("Do not get angry, man (v0.3 ALPHA)")));
	mainLayout = new QHBoxLayout(ui.centralWidget);
	gameBoard = NULL;
	rightLayout = NULL;
	textBox = NULL;
	nextPlayerButton = NULL;
		
	ui.actionSave_game->setEnabled(false);
	ui.actionSave_game_as->setEnabled(false);
	ui.actionLeave_network_game->setVisible(false);

	connect(ui.actionQuit, SIGNAL(triggered()), app, SLOT(quit()));
	connect(ui.actionNew_game, SIGNAL(triggered()), this, SLOT(newGameSlot()));
	connect(ui.actionSave_game, SIGNAL(triggered()), this, SLOT(saveGameSlot()));
	connect(ui.actionSave_game_as, SIGNAL(triggered()), this, SLOT(saveGameAsSlot()));
	connect(ui.actionLoad_game, SIGNAL(triggered()), this, SLOT(loadGameSlot()));
	connect(ui.actionGame_settings, SIGNAL(triggered()), this, SLOT(gameSettingsSlot()));
	connect(ui.actionManual, SIGNAL(triggered()), this, SLOT(showManualSlot()));
	connect(ui.actionAbout, SIGNAL(triggered()), this, SLOT(showAboutSlot()));
	connect(ui.actionNew_network_game, SIGNAL(triggered()), this, SLOT(newNetworkGameSlot()));
	connect(ui.actionLeave_network_game, SIGNAL(triggered()), this, SLOT(leaveNetworkGameSlot()));

	networkGame = NULL;
	lobby = NULL;
	loginNetwork = NULL;
	this->dice = NULL;
	gameSettings = new GameSettings(this, "settings.xml");
	if(!(QDir(gameSettings->getTemplateDir()).exists())){
		QMessageBox::critical(this, tr("Loading error"),  tr("Missing template dir. Change your template in settings, otherwise your graphic will not show correctly."));
	}
	settingDialog = new Settings(this, gameSettings);
	//newGameSlot();
}

hra2011::~hra2011()
{
	delete mainLayout;
}



void hra2011::nextPlayer(){
	if(!this->isNetworkGame){
		players.at(onMove)->setNextJump(-1);
		if(!actualPlayerSheet.isEmpty())
			this->lblPlayers.at(onMove)->setStyleSheet(actualPlayerSheet);

		// pokud nepadla 6 a je zapnut� automatick� p�ep�n�n� m�du, tak po ub�hnut� nastaven�ho po�tu kol p�epn� m�d
		if(dice->getActualValue() != 6){
			if(gameSettings->getAutomaticSwitch() && onMove == 0){
				automaticSwitchCount++;
				if(automaticSwitchCount%gameSettings->getAutomaticSwitchNumber() == 0){
					// zmena modu
					this->computerModeNext();
				}
			}
			onMove = (onMove+1)%numOfPlayers;
		}
		// spo��t� hr��e, kte�� ji� dohr�li
		int playersFinished = 0;
		while(players.at(onMove)->finished() && playersFinished != numOfPlayers){
			onMove = (onMove+1)%numOfPlayers;
			playersFinished++;
		}
		if(playersFinished == numOfPlayers){
			//konec hry
			hra2011::gameFinish();
		}else{
			QString myText = "hraje: "+players.at(onMove)->getPlayerName();
			this->textBox->appendPlainText(myText);

			actualPlayerSheet = this->lblPlayers.at(onMove)->styleSheet();
			this->lblPlayers.at(onMove)->setStyleSheet(actualPlayerSheet+" border-color: white;");
			myText = "hozeno: "+ QString::number(dice->throwDice());
			this->textBox->appendPlainText(myText);
			players.at(onMove)->setNextJump(dice->getActualValue());
		}
	}
}

void hra2011::saveGameAsSlot(){
	this->saveFile = QFileDialog::getSaveFileName(this,
		tr("Save game as..."), QDir::currentPath()+"\\"+gameSettings->getDefaultSaveDir()+"\\", tr("Game files (*.xml)"));

	hra2011::saveGame(this->saveFile);
	ui.actionSave_game->setEnabled(true);
}

void hra2011::saveGameSlot(){
	if(!this->saveFile.isEmpty())
		hra2011::saveGame(this->saveFile);
}

void hra2011::saveGame(QString fileName){


	QFile outFile(fileName);
	 if (!outFile.open(QIODevice::WriteOnly | QIODevice::Text))
		 return;

	 QTextStream out(&outFile);
	 out << this->makeSaveGame() << "\n";
	 outFile.close();
}

void hra2011::loadGameSlot(){
	saveFile = QFileDialog::getOpenFileName(this,
     tr("Load game"), QDir::currentPath()+"\\"+gameSettings->getDefaultSaveDir()+"\\", tr("Game files (*.xml)"));
	if(!saveFile.isEmpty()){
		QFile gameFile(saveFile);
		 if (!gameFile.open(QIODevice::ReadOnly | QIODevice::Text))
			 return;

		QDomDocument doc("clovece"); 
		doc.setContent(&gameFile);
		QDomElement xClovece = doc.firstChildElement("clovece"); 
		int numOfPlayers;
		QVector<QVector<int> > *playersPosition;
		if(!xClovece.isNull()){
			// �ten� nastaven� hry
			QDomElement xSettings = xClovece.firstChildElement("settings");
			while(!xSettings.isNull()){
				numOfPlayers=xSettings.attribute("numOfPlayers").toInt();
				QString mapFile=xSettings.attribute("mapFile");
				int onMove=xSettings.attribute("onMove").toInt();
				settingDialog->loadSettings(numOfPlayers, mapFile, onMove);
				xSettings=xSettings.nextSiblingElement("settings"); 
			}

			// �ten� hern� desky
			QDomElement xGameBoard = xClovece.firstChildElement("gameboard");
			while(!xGameBoard.isNull()){
				settingDialog->loadCroftValues(xGameBoard.text().trimmed());
				xGameBoard=xGameBoard.nextSiblingElement("gameboard"); 
			}

			// �ten� elementu s hr��em
			playersPosition = new QVector<QVector<int> > (numOfPlayers, QVector<int>(4));
			QDomElement xPlayers = xClovece.firstChildElement("player");
			int i = 0;
			while(!xPlayers.isNull()){
				QString playerName=xPlayers.attribute("name");
				int boardPosition=xPlayers.attribute("boardPosition").toInt();
				int playerType=xPlayers.attribute("playerType").toInt();
				int computerMode=xPlayers.attribute("computerMode", 0).toInt();
				int colorR=xPlayers.attribute("colorR", 0).toInt();
				int colorG=xPlayers.attribute("colorG", 0).toInt();
				int colorB=xPlayers.attribute("colorB", 0).toInt();
				QColor *color = new QColor(colorR, colorG, colorB);
				settingDialog->addPlayer(playerName, playerType, color, boardPosition, computerMode);
				// �ten� elementu figurky
				QDomElement xPiece = xPlayers.firstChildElement("piece");
				int j = 0;
				while(!xPiece.isNull()){
					(*playersPosition)[i][j] = xPiece.attribute("actualPosition").toInt();
					xPiece=xPiece.nextSiblingElement("piece"); 
					j++;
				}
				xPlayers=xPlayers.nextSiblingElement("player"); 
				i++;
			}
		}
			
		ui.actionSave_game->setEnabled(true);
		this->numOfPlayers = settingDialog->playerNum;
		newGame(settingDialog->mapFilePath, this->numOfPlayers);
		//vyplneni puvodnima hodnotama
		gameBoard->loadCroftValues(settingDialog->croftValues);
		//nastaven� figurek na spr�vn� pozice
		for(int i = 0; i<playersPosition->size(); i++){
			for(int j = 0; j<4; j++){
				int x = (*playersPosition)[i][j];
				if(x>=4)
					players.at(i)->setPieceTo(j, x);
			}
		}
		//nastaven� kdo je na �ad�
		if(numOfPlayers > 1){
			if(settingDialog->onMove == 0)
				this->onMove = this->numOfPlayers-1;
			else
				this->onMove = settingDialog->onMove-1;
			hra2011::nextPlayer();
		}
	}

}

void hra2011::newGameSlot(){
	if(settingDialog->exec()){
			this->numOfPlayers = settingDialog->playerNum;
			newGame(settingDialog->mapFilePath, this->numOfPlayers);
	}
}


void hra2011::newGame(QString mapFile, int numOfPlayers, bool isNetwork){
	this->mapFile = mapFile;
	this->numOfPlayers = numOfPlayers;

	// odstrani starou hru
	if(this->gameBoard != NULL) 
		delete this->gameBoard;
	for(int i = 0; i < players.size(); i++){
		delete comboComputerMode.at(i);
		delete players.at(i);
		delete lblPlayers.at(i);		
	}
	if(this->rightLayout != NULL)
		delete this->rightLayout;
	if(this->textBox != NULL)
		delete this->textBox;
	if(this->nextPlayerButton != NULL)
		delete this->nextPlayerButton;
	if(this->dice != NULL)
		delete this->dice;
	lblPlayers.erase(lblPlayers.begin(), lblPlayers.end());
	players.erase(players.begin(), players.end());
	comboComputerMode.erase(comboComputerMode.begin(), comboComputerMode.end());
	this->automaticSwitchCount = 0;

	// vytvori novou plochu a nove hrace
	gameBoard = new GameBoard(this, mapFile, numOfPlayers, gameSettings->getTemplateDir());
	this->onMove = numOfPlayers-1;
	for(int i = 0; i<numOfPlayers; i++){
		players.append(new Player(this, gameBoard, settingDialog->getPlayerSetting(i)->getBoardPosition(), settingDialog->getPlayerSetting(i)->getPlayerType(), gameSettings->getTemplateDir(), settingDialog->getPlayerSetting(i)->getPlayerColor(), settingDialog->getPlayerSetting(i)->getPlayerName()));
		comboComputerMode.append(new QComboBox(this));
		QComboBox *tmpCombo = comboComputerMode.last();
		if(settingDialog->getPlayerSetting(i)->getPlayerType() == Player::COMPUTER){
			players.last()->setComputerMode(settingDialog->getPlayerSetting(i)->getComputerMode());
			tmpCombo->addItem(tr("Random"));
			tmpCombo->addItem(tr("Largest move"));
			tmpCombo->addItem(tr("Prefer to get home"));
			tmpCombo->addItem(tr("Prefer to get on"));
			int tmp = static_cast<int>(settingDialog->getPlayerSetting(i)->getComputerMode());
			tmpCombo->setCurrentIndex(tmp);
			connect(tmpCombo, SIGNAL(activated(int)), this, SLOT(computerModeChanged(int)));
		}else{
			tmpCombo->addItem(tr("HUMAN"));
			tmpCombo->setEnabled(false);
		}
		lblPlayers.append(new QLabel(players.last()->getPlayerName(),this));
		lblPlayers.last()->setStyleSheet("background-color: rgb("+QString::number(settingDialog->getPlayerSetting(i)->getPlayerColor()->red())+", "+QString::number(settingDialog->getPlayerSetting(i)->getPlayerColor()->green())+", "+QString::number(settingDialog->getPlayerSetting(i)->getPlayerColor()->blue())+");\
			padding: 4px; border-style: outset; border-width: 4px; color: white;");
	}
	rightLayout = new QVBoxLayout;	
	computerModeLayout = new QFormLayout(this);
	nextPlayerButton = new QPushButton(tr("Next player"));
	dice = new Dice(this, gameSettings->getTemplateDir());
	textBox = new QPlainTextEdit(this);
	for(int i = 0; i<numOfPlayers; i++){
			computerModeLayout->addRow(lblPlayers.at(i), comboComputerMode.at(i));
	}
	connect(nextPlayerButton, SIGNAL(clicked()), this, SLOT(nextPlayer()));

	// um�st� ovl�dac� prvky a hern� plochu
	mainLayout->addWidget(gameBoard);
	mainLayout->addLayout(rightLayout);
	rightLayout->addWidget(nextPlayerButton);
	rightLayout->addLayout(computerModeLayout);
	rightLayout->addWidget(dice);
	rightLayout->addWidget(textBox);
	this->isNetworkGame = isNetwork;

	ui.actionSave_game_as->setEnabled(true);
	//hra2011::nextPlayer();
}


void hra2011::playerFinishedSlot(){
// save actual name for statistics
	this->ranks.append(players.at(onMove)->getPlayerName());
	QString myText = "Player "+players.at(onMove)->getPlayerName()+" finished.";
	this->textBox->appendPlainText(myText);
}

void hra2011::gameFinish(){
	QString order(tr("GAME FINISHED\n\n"));
	order += this->ranks.at(0)+" is the winner.\n\n";
	for(int i = 0; i<numOfPlayers; i++){
		order += QString::number(i+1)+". "+this->ranks.at(i)+"\n";
	}
	
	QMessageBox::information(this, tr("GAME FINISHED"),  order);

	QString myText = "GAME FINISHED!!";
	this->textBox->appendPlainText(myText);
}

void hra2011::gameSettingsSlot(){
	if(gameSettings->exec()){
		// provede nastaven�
	}
}

void hra2011::showManualSlot(){
	QFile file(QDir::currentPath()+"/"+gameSettings->getManualFile());
	if(file.exists())
		QDesktopServices::openUrl(QUrl("file:///"+QDir::currentPath()+"/"+gameSettings->getManualFile(), QUrl::TolerantMode));
	else
		QMessageBox::warning(this, tr("Opening error"),  tr("Missing manual file, check file path in settings."));
}
void hra2011::showAboutSlot(){
	QMessageBox::information(this, tr("About ..."),  tr("Do not get angry, man!\nICP 2011 project\nAuthors: Tomas Lipovsky (xlipov02) & Tomas Ondrouch (xondro06)"));
}

void hra2011::playedWith(int pieceNumber){
	if(this->isNetworkGame){

		onMove = (onMove+1)%numOfPlayers;


		emit networkGame->jump(onMove, dice->throwDice(), pieceNumber);

	}
}
void hra2011::newNetworkGameSlot(){
	if(networkGame == NULL){
	// u�ivatel nen� p�ipojen k serveru
		loginNetwork = new Login(this, gameSettings);
		connect(this, SIGNAL(connected()), loginNetwork, SLOT(gotConnected()));
		connect(this, SIGNAL(registered()), loginNetwork, SLOT(registeredSlot()));
		connect(this, SIGNAL(notreged()), loginNetwork, SLOT(notregedSlot()));
		connect(this, SIGNAL(logged()), this, SLOT(setNetworkClass()));
		connect(this, SIGNAL(logged()), loginNetwork, SLOT(loggedSlot()));
		connect(this, SIGNAL(wronglogin()), loginNetwork, SLOT(wrongLoginSlot()));
		if(loginNetwork->exec()){
			// game loby
			ui.actionNew_network_game->setVisible(false);
			ui.actionLeave_network_game->setVisible(true);
			lobby = new NetworkLobby(this, gameSettings, networkGame);
			connect(networkGame, SIGNAL(gameStarted(QString)), this, SLOT(networkGameLoad(QString)));
			connect(networkGame, SIGNAL(jumped(int, int, int)), this, SLOT(networkReceivedInfo(int, int, int)));
			if(lobby->exec()){
				// spusti hru

				// odstrani starou hru
				if(this->gameBoard != NULL) 
					delete this->gameBoard;
				for(int i = 0; i < players.size(); i++){
					delete comboComputerMode.at(i);
					delete players.at(i);
					delete lblPlayers.at(i);		
				}
				if(this->rightLayout != NULL)
					delete this->rightLayout;
				if(this->textBox != NULL)
					delete this->textBox;
				if(this->nextPlayerButton != NULL)
					delete this->nextPlayerButton;
				if(this->dice != NULL)
					delete this->dice;
				lblPlayers.erase(lblPlayers.begin(), lblPlayers.end());
				players.erase(players.begin(), players.end());
				comboComputerMode.erase(comboComputerMode.begin(), comboComputerMode.end());


				this->numOfPlayers = lobby->connectedPlayers;
				this->mapFile = lobby->mapFilePath;
				// vytvori novou plochu a nove hrace
				gameBoard = new GameBoard(this, lobby->mapFilePath, numOfPlayers, gameSettings->getTemplateDir());
				this->onMove = numOfPlayers-1;
				for(int i = 0; i < numOfPlayers; i++){
					players.append(new Player(this, gameBoard, lobby->getPlayerSetting(i)->getBoardPosition(), lobby->getPlayerSetting(i)->getPlayerType(), gameSettings->getTemplateDir(), lobby->getPlayerSetting(i)->getPlayerColor(), lobby->getPlayerSetting(i)->getPlayerName()));
					comboComputerMode.append(new QComboBox(this));
					comboComputerMode.last()->addItem("HUMAN");
					comboComputerMode.last()->setEnabled(false);
					lblPlayers.append(new QLabel(players.last()->getPlayerName(),this));
					lblPlayers.last()->setStyleSheet("background-color: rgb("+QString::number(lobby->getPlayerSetting(i)->getPlayerColor()->red())+", "+QString::number(lobby->getPlayerSetting(i)->getPlayerColor()->green())+", "+QString::number(lobby->getPlayerSetting(i)->getPlayerColor()->blue())+");\
						padding: 4px; border-style: outset; border-width: 4px; color: white;");
				}
				rightLayout = new QVBoxLayout;	
				computerModeLayout = new QFormLayout(this);
				nextPlayerButton = new QPushButton(tr("Next player"));
				dice = new Dice(this, gameSettings->getTemplateDir());
				textBox = new QPlainTextEdit(this);
				for(int i = 0; i<numOfPlayers; i++){
						computerModeLayout->addRow(lblPlayers.at(i), comboComputerMode.at(i));
				}
				connect(nextPlayerButton, SIGNAL(clicked()), this, SLOT(nextNetPlayer()));

				// um�st� ovl�dac� prvky a hern� plochu
				mainLayout->addWidget(gameBoard);
				mainLayout->addLayout(rightLayout);
				rightLayout->addWidget(nextPlayerButton);
				rightLayout->addLayout(computerModeLayout);
				rightLayout->addWidget(dice);
				rightLayout->addWidget(textBox);


				//vytvo�� save pro ostatn�
				QString saveForOthers = this->makeSaveGame();

				this->isNetworkGame = true;
				networkGame->startGame(saveForOthers);

					// nastav� se jako lok�ln� hr��
				for(int i = 0; i < this->numOfPlayers; i++){
					if(lobby->getPlayerSetting(i)->getBoardPosition() == lobby->localPlayerBoardPosition)
						players.at(i)->setPlayerType(Player::HUMAN);
					else
						players.at(i)->setPlayerType(Player::LAN);
				}

				// za�ne hr�t
			QString myText = "hraje: "+players.at(onMove)->getPlayerName();
			this->textBox->appendPlainText(myText);

			actualPlayerSheet = this->lblPlayers.at(onMove)->styleSheet();
			this->lblPlayers.at(onMove)->setStyleSheet(actualPlayerSheet+" border-color: white;");
			myText = "hozeno: "+ QString::number(dice->throwDice());
			this->textBox->appendPlainText(myText);
			players.at(onMove)->setNextJump(dice->getActualValue());

			}
		}
		delete loginNetwork;
	}
}
void hra2011::leaveNetworkGameSlot(){
	if(networkGame != NULL){
	// odhl�en� od hry

	// odhl�en� od serveru
		
	// zru�en� s�ov� t��dy
		delete networkGame;
		networkGame = NULL;
		ui.actionJoin_network_game->setVisible(true);
		ui.actionNew_network_game->setVisible(true);
		ui.actionLeave_network_game->setVisible(false);
	}
}

void hra2011::setNetworkClass(){
	if(loginNetwork != NULL)
		this->networkGame = loginNetwork->getNetwork();
}
void hra2011::computerModeChanged(int index){
	for(int i = 0; i<numOfPlayers; i++){
		if(players.at(i)->getPlayerType() == Player::COMPUTER)
			players.at(i)->setComputerMode(static_cast<Player::ComputerMode>(comboComputerMode.at(i)->currentIndex()));
	}
}

void hra2011::computerModeNext(){
	for(int i = 0; i<numOfPlayers; i++){
		if(players.at(i)->getPlayerType() == Player::COMPUTER)
			comboComputerMode.at(i)->setCurrentIndex((comboComputerMode.at(i)->currentIndex()+1)%4);
			players.at(i)->setComputerMode(static_cast<Player::ComputerMode>(comboComputerMode.at(i)->currentIndex()));
	}
}

QString hra2011::makeSaveGame(){

	QString result;
	result = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<clovece>\n";
	result += "<settings numOfPlayers=\"";
	result += this->numOfPlayers+'0'; //QString::number(this->numOfPlayers);
	result += "\" mapFile=\""+this->mapFile+"\" ";
	result += "onMove=\"";
	result += this->onMove+'0'; //QString::number(this->onMove);
	result += "\" />\n";

	result += this->gameBoard->saveGameGameboard();

	for(int i = 0; i<numOfPlayers; i++){
		result += players.at(i)->saveGamePlayer();
	}
	result += "</clovece>\n";

	return result;
}

void hra2011::makeLoadGame(QString savedGame){
		QDomDocument doc("clovece"); 
		doc.setContent(savedGame);
		QDomElement xClovece = doc.firstChildElement("clovece"); 
		int numOfPlayers;
		QVector<QVector<int> > *playersPosition;
		if(!xClovece.isNull()){
			// �ten� nastaven� hry
			QDomElement xSettings = xClovece.firstChildElement("settings");
			while(!xSettings.isNull()){
				numOfPlayers=xSettings.attribute("numOfPlayers").toInt();
				QString mapFile=xSettings.attribute("mapFile");
				int onMove=xSettings.attribute("onMove").toInt();
				settingDialog->loadSettings(numOfPlayers, mapFile, onMove);
				xSettings=xSettings.nextSiblingElement("settings"); 
			}

			// �ten� hern� desky
			QDomElement xGameBoard = xClovece.firstChildElement("gameboard");
			while(!xGameBoard.isNull()){
				settingDialog->loadCroftValues(xGameBoard.text().trimmed());
				xGameBoard=xGameBoard.nextSiblingElement("gameboard"); 
			}

			// �ten� elementu s hr��em
			playersPosition = new QVector<QVector<int> > (numOfPlayers, QVector<int>(4));
			QDomElement xPlayers = xClovece.firstChildElement("player");
			int i = 0;
			while(!xPlayers.isNull()){
				QString playerName=xPlayers.attribute("name");
				int boardPosition=xPlayers.attribute("boardPosition").toInt();
				int playerType=xPlayers.attribute("playerType").toInt();
				int computerMode=xPlayers.attribute("computerMode", 0).toInt();
				int colorR=xPlayers.attribute("colorR", 0).toInt();
				int colorG=xPlayers.attribute("colorG", 0).toInt();
				int colorB=xPlayers.attribute("colorB", 0).toInt();
				QColor *color = new QColor(colorR, colorG, colorB);
				settingDialog->addPlayer(playerName, playerType, color, boardPosition, computerMode);
				// �ten� elementu figurky
				QDomElement xPiece = xPlayers.firstChildElement("piece");
				int j = 0;
				while(!xPiece.isNull()){
					(*playersPosition)[i][j] = xPiece.attribute("actualPosition").toInt();
					xPiece=xPiece.nextSiblingElement("piece"); 
					j++;
				}
				xPlayers=xPlayers.nextSiblingElement("player"); 
				i++;
			}
		}
			
		ui.actionSave_game->setEnabled(true);
		this->numOfPlayers = settingDialog->playerNum;
		newGame(settingDialog->mapFilePath, this->numOfPlayers, true);
		//vyplneni puvodnima hodnotama
		gameBoard->loadCroftValues(settingDialog->croftValues);
		//nastaven� figurek na spr�vn� pozice
		for(int i = 0; i<playersPosition->size(); i++){
			for(int j = 0; j<4; j++){
				int x = (*playersPosition)[i][j];
				if(x>=4)
					players.at(i)->setPieceTo(j, x);
			}
		}
		//nastaven� kdo je na �ad�
		if(numOfPlayers > 1){
			if(settingDialog->onMove == 0)
				this->onMove = this->numOfPlayers-1;
			else
				this->onMove = settingDialog->onMove-1;
			hra2011::nextPlayer();
		}
}


void hra2011::networkReceivedInfo(int onMove, int value, int lastPiece){
	// ��slo lok�ln�ho hr��e
	int localPlayer = lobby->localPlayerBoardPosition;


	// posune figurku p�edchoz�ho hr��e
	if(lastPiece != -1)
		players.at(this->onMove)->jumpNextWithPiece(lastPiece);

	// zru�� zv�razn�n� p�edchoz�ho hr��e
	//players.at(this->onMove)->setNextJump(-1);
	if(!actualPlayerSheet.isEmpty())
		this->lblPlayers.at(this->onMove)->setStyleSheet(actualPlayerSheet);

	//nastav� novou hodnotu kostky a kdo je aktu�ln� na �ad�
	this->dice->setActualValue(value);
	this->onMove = onMove;

	// spo��t� hr��e, kte�� ji� dohr�li
	int playersFinished = 0;
	while(players.at(onMove)->finished() && playersFinished != numOfPlayers){
		onMove = (onMove+1)%numOfPlayers;
		playersFinished++;
	}
	if(playersFinished == numOfPlayers){
		//konec hry
		hra2011::gameFinish();
	}else{
		QString myText = "hraje: "+players.at(onMove)->getPlayerName();
		this->textBox->appendPlainText(myText);

		actualPlayerSheet = this->lblPlayers.at(this->onMove)->styleSheet();
		this->lblPlayers.at(onMove)->setStyleSheet(actualPlayerSheet+" border-color: white;");
		myText = "hozeno: "+ QString::number(dice->getActualValue());
		this->textBox->appendPlainText(myText);

		if(onMove == localPlayer)
			players.at(onMove)->setNextJump(dice->getActualValue());
	}
}

void hra2011::networkGameLoad(const QString &xmlFile){
	// na�te hru
	this->isNetworkGame = true;
	this->makeLoadGame(xmlFile);
	this->lobby->reject();

	// nastav� se jako lok�ln� hr��
	for(int i = 0; i < this->numOfPlayers; i++){
		if(settingDialog->getPlayerSetting(i)->getBoardPosition() == lobby->localPlayerBoardPosition)
			players.at(i)->setPlayerType(Player::HUMAN);
		else
			players.at(i)->setPlayerType(Player::LAN);
	}

}
void hra2011::nextNetPlayer(){
	playedWith(-1);
}