/** @file player.h
 *  @brief Modul pro pr�ci s hr��em
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include "gameboard.h"
#include "piece.h"

class Croft;

/** 
 *  @brief T��da reprezentuj�c� hr��e
 *  @author Tom� Lipovsk�
 *  @version 0.3
 *  @date 2011-05-05
 */
class Player : public QObject
{
	Q_OBJECT

public:

	/** 
	 * @brief Typ hr��e
	 */
	enum PlayerType {
		HUMAN, /**< @brief �lov�k */
		COMPUTER, /**< @brief Po��ta� */
		LAN /**< @brief S�ov� hr�� */
	};

	/** 
	 * @brief Typ rozhodovac�ho mechanizmus
	 */
	enum ComputerMode {
		RANDOM, /**< @brief Tah n�hodnou figurkou */
		LONGEST, /**< @brief Tah s nejlep�� metrikou - nejdel�� tah */
		NEXTHOME, /**< @brief Preferuje skok do dome�ku */
		GETOUT /**< @brief Preferuje nasazen� */
	};


	Player(QObject *parent, GameBoard *board, const int playerNumber, PlayerType playerType, QString imageDir, QColor *color, QString playerName="");
	~Player();

	/**
	 * @brief Vr�t� ��slo hr��e
	 * @return ��slo hr��e
	 */
	int getPlayerNumber();

	/**
	 * @brief Vr�t� jm�no hr��e
	 * @return Jm�no hr��e
	 */
	QString getPlayerName();

	/**
	 * @brief Nastav� v�em figurk�m jejich mo�n� skoky a zv�razn� je
	 *
	 * Pokud je na tahu po��ta�, spust� se um�l� inteligence.
	 * @param value Hozen� hodnota, na kterou m��ou figurky sko�it
	 * @see Piece::setNextJump, disableNextJump
	 */
	void setNextJump(int value=-1);

	/**
	 * @brief Vr�t� pol��ko na zadan� pozici (z pohledu hr��ovi cesty)
	 * @param position Pozice, na kter� se hled� pol��ko
	 * @return Pol��ko na hledan� pozici
	 */
	Croft* getCroftAt(int position);

	/**
	 * @brief Vr�t� ��st XML s nastaven�m hr��e a jeho figurek
	 * @return Nastaven� hr��e
	 * @see Piece::saveGamePiece, hra2011::saveGame
	 */
	QString saveGamePlayer();

	/**
	 * @brief Vr�t� typ hr��e
	 * @return Typ hr��e z v��tu PlayerType
	 * @see PlayerType
	 */
	int getPlayerType();

	/**
	 * @brief Vr�t� true, pokud m� hr�� v�echny figurky v dome�ku
	 * @return Vr�t� true, pokud hr�� dohr�l, jinak false
	 */
	bool finished();

	/**
	 * @brief Nastaven� hern� m�d po��ta�e
	 * @param mode Hern� m�d
	 */
	void setComputerMode(ComputerMode mode);

	/**
	 * @brief Posune figurku na danou pozici
	 * @param piece ��slo figurky
	 * @param newPosition ��slo nov� pozice
	 */
	void setPieceTo(int piece, int newPosition);

	/**
	 * @brief Sko�� se zadanou figurkou na dal�� mo�nou pozici
	 * @param piece ��slo figurky
	 * @return Vr�t� true, pokud se skok povedl
	 */
	bool jumpNextWithPiece(int piece);

	void setPlayerType(PlayerType playerType);
signals:
	/**
	 * @brief Ukon�� kolo
	 */
	void finishTurn();

	/**
	 * @brief Hr�� pr�v� dohr�l
	 */
	void playerFinished();

	/**
	 * @brief Hr�� hr�l s figurkou
	 * @param pieceNumber ��slo figurky
	 */
	void playedWith(int pieceNumber);
public slots:
	/**
	 * @brief Zru�� v�em figurk�m jejich mo�n� skoky a vyvol� sign�l o konci kola.
	 * @see setNextJump
	 */
	void disableNextJump();
private:
	Path *playerPath;
	int playerNumber;
	QList<Piece *> pieces;
	QString playerName;
	QPixmap *playerAvatar;
	QColor *playerColor;
	PlayerType playerType;
	ComputerMode computerMode;

	int piecesAtHome;
	void computerMod();
};

#endif // PLAYER_H
