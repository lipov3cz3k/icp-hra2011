/** @file piece.cpp
 *  @brief Modul pro pr�ci s figurkou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "piece.h"

Piece::Piece(QObject *parent, Path* playerPath, const int playerNumber, const int startPosition, QColor *playerColor, QPixmap *playerAvatar)
	: QObject(parent)
{
	this->playerNumber = playerNumber;
	// nasadi figurku do OUT
	this->playerPath = playerPath;
	this->startPosition = startPosition;
	this->actualPosition = startPosition;
	this->nextPosition = -1;
	this->playerAvatar = playerAvatar;
	this->playerColor = playerColor;
	Piece::jumpAt(this->startPosition);
	connect(this, SIGNAL(disableOtherPieces()), parent, SLOT(disableNextJump()));
	connect(this, SIGNAL(playedWith(int)), parent, SIGNAL(playedWith(int)));
}

Piece::~Piece()
{

}

bool Piece::jumpAt(int position){
	if(position >= 0){
		Croft* oldCroft = this->playerPath->at(actualPosition);
		Croft* newCroft = this->playerPath->at(position);
		int newUsedBy = newCroft->isUsedBy();
		if(newUsedBy){
			newCroft->pieceOut();
		}
		oldCroft->setUsedBy(NULL, 0, NULL);
		newCroft->setUsedBy(this, this->playerNumber, playerAvatar);
		this->actualPosition = position;
		return true;
	}else
		return false;
}

void Piece::setNextJump(int value){
	if(value>=0){
		int possibleNext;
		if(value == 6 && actualPosition < 4)
			// nasazeni na hozenou hodnotu 6
			possibleNext = 4;
		else
			if((possibleNext = this->playerPath->getNextJump(this->actualPosition, value)) == 4)
				// dalsi pozice je nasazovaci policko
				possibleNext = -1;

		if( ((actualPosition < 4 && possibleNext == 4) || (actualPosition >= 4 && (possibleNext < this->playerPath->size()) )) && possibleNext >=0  && this->playerPath->at(possibleNext)->isUsedBy() != this->playerNumber){
			// figurka je jeste na stridace, nebo muze hrat a neni mimo pole
			nextPosition = possibleNext;
			Piece::highLightNext(true, this->playerColor);
		}else{
			nextPosition = -1;
		}

	}else{
		Piece::highLightNext(false, this->playerColor);
		 this->nextPosition = -1;
	}
}

void Piece::highLightNext(bool type, QColor *playerColor){
	if(this->nextPosition>=0)
			this->playerPath->at(this->nextPosition)->highLight(type, playerColor);
}

bool Piece::jumpToNextValue(){
	return Piece::jumpAt(this->nextPosition);
}

int Piece::getNextMatric(){
	if(this->nextPosition >=0)
		return this->nextPosition - this->actualPosition;
	else
		return -1;
}

bool Piece::isNextHome(){
	if(this->nextPosition >= 0 && this->playerPath->at(this->nextPosition)->getType() == Croft::HOME)
		return true;
	else
		return false;
}

bool Piece::isHome(){
	if(this->playerPath->at(this->actualPosition)->getType() == Croft::HOME)
		return true;
	else
		return false;
}

bool Piece::isOut(){
	if(this->actualPosition < 4)
		return true;
	else
		return false;
}

void Piece::croftClicked(){
	if(nextPosition>=0){
		if(Piece::jumpAt(this->nextPosition)){
			Piece::highLightNext(false, this->playerColor);
			emit Piece::playedWith(this->startPosition);
			emit Piece::disableOtherPieces();
		}
	}
}

void Piece::pieceOut(){
	Piece::jumpAt(this->startPosition);
}

QString Piece::saveGamePiece(){
	QString result;
	result = "\t\t<piece actualPosition=\""+QString::number(actualPosition)+"\" ";
	result += "startPosition=\""+QString::number(startPosition)+"\" />\n";
	return result;
}