/** @file playersettings.h
 *  @brief Modul s nastaven�m hr��e
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef PLAYERSETTINGS_H
#define PLAYERSETTINGS_H

#include <QObject>
#include "player.h"

/** 
 *  @brief T��da s nastaven�m hr��e
 *  @author Tom� Lipovsk�
 *  @version 0.1
 *  @date 2011-05-05
 */
class PlayerSettings : public QObject
{
	Q_OBJECT

public:
	/**
	 * @brief Vytvo�� nastaven� pro hr��e
	 * @param parent Rodi�ovsk� QObject
	 * @param playerName Jm�no hr��e
	 * @param playerType Typ hr��e
	 * @param playerColor Barva hr��e
	 * @param boardPosition Pozice na hern� desce
	 * @param computerMode Typ hern�ho m�du po��ta�e
	 */
	PlayerSettings(QObject *parent, QString playerName, Player::PlayerType playerType, QColor *playerColor, int boardPosition, int computerMode);

	/**
	 * @brief Destruktor nastaven� hr��e
	 */
	~PlayerSettings();

	/**
	 * @brief Vr�t� typ hr��e
	 * @return Typ hr��e
	 */
	Player::PlayerType getPlayerType();

	/**
	 * @brief Vr�t� jm�no hr��e
	 * @return Jm�no hr��e
	 */
	QString getPlayerName();

	/**
	 * @brief Vr�t� ��slo pozice na hern� desce
	 * @return ��slo pozice
	 */
	int getBoardPosition();

	/**
	 * @brief Vr�t� typ hern�ho m�du po��ta�e
	 * @return Hern� m�d
	 */
	Player::ComputerMode getComputerMode();
	
	/**
	 * @brief Vr�t� barvu hr��e
	 * @return Barva hr��e
	 */
	QColor* getPlayerColor();

private:
	QString playerName;
	Player::PlayerType playerType;
	QColor *playerColor;
	int computerMode;
	int boardPosition;
	
};

#endif // PLAYERSETTINGS_H
