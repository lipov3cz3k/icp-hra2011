/** @file networklobby.h
 *  @brief Modul pro lobby s�ov� hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef NETWORKLOBBY_H
#define NETWORKLOBBY_H

#include <QObject>
#include <QDialog>
#include "ui_networklobby.h"
#include "gamesettings.h"
#include "network.h"
#include "playersettings.h"
#include "player.h"

class NetworkLobby : public QDialog
{
	Q_OBJECT

public:
	NetworkLobby(QWidget *parent, GameSettings *gameSettings, Network *network);
	~NetworkLobby();

	/**
	 * @brief Vr�t� nastaven� konkr�tn�ho hr��e
	 * @param i ��slo hr��e
	 * @return Objekt s nastavne�m hr��e
	 */
	PlayerSettings* getPlayerSetting(int i);

	QString mapFilePath;
	QVector<PlayerSettings *> players;
	int localPlayerBoardPosition;
	int connectedPlayers;


public slots:
	void fillLoadListSlot(const QStringList &gameList);
	void loadPlayersSlot(const QList <QPair<QString, int> > playerList);
	void enableCreateGame(const bool &canCreate);


private slots:
	void loadMapSlot();
	void on_loadGameGroup_toggled(bool state);
	void on_makeGameGroup_toggled(bool state);
	void on_actualizeLoadButton_clicked();
	void on_createGameButton_clicked();
	void on_acceptButton_clicked();
	void on_cancelButton_clicked();
	void sitSlot();

private:
	Ui::NetworkLobby ui;
	GameSettings *gameSettings;
	Network *network;
	int maxPlayers;
	QVector<QLineEdit *> playerNameEdit;
	QVector<QPushButton *> sitButton;
	

};

#endif // NETWORKLOBBY_H
