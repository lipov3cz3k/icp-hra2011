/** @file croft.cpp
 *  @brief Modul pro pr�ci s hern�m pol��kem.
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 *
 */

#include "stdafx.h"
#include "croft.h"

Croft::Croft(QWidget *parent, FieldType type, int pos_x, int pos_y)
	: QWidget(parent)
{
	pen = new QPen();
	brush = new QBrush(Qt::SolidPattern);
	valueImg = NULL;
	avatarImg = NULL;
	this->type = type;
	this->value = -1;
	setType(type);
	this->value = -1;
	setPosition(pos_x, pos_y);
	usedBy = NULL;
	usedByPlayer = 0;
}

Croft::~Croft()
{
	delete this->valueImg;
	delete this->brush;
	delete this->pen;
}

void Croft::paintEvent(QPaintEvent *event){
	QPainter painter(this);

	painter.setBrush(*brush);
	painter.setPen(*pen);
	painter.setCompositionMode(QPainter::CompositionMode_SourceAtop);
	QRect rect = QRect(1,1, 28, 28);
	
	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawEllipse(rect);
	if(valueImg != NULL)
		painter.drawPixmap(rect, *valueImg);
	if(avatarImg != NULL){
		painter.drawPixmap(rect, *avatarImg);
	}
}

void Croft::setType(FieldType type){
	this->type = type;
	QString cesta="";
	switch(type){
		case OUT:
			this->pen->setColor(QColor(224, 224, 224));
			this->brush->setColor(QColor(224, 224, 224));
		break;
		case HOME:
			this->pen->setColor(QColor(224, 224, 224));
			this->brush->setColor(QColor(224, 224, 224));
		break;
		case START:
			this->pen->setColor(Qt::black);
			this->pen->setWidth(3);
			this->brush->setColor(Qt::transparent);
		break;
		case NORMAL:
			this->pen->setColor(Qt::black);
			this->brush->setColor(Qt::transparent);
		break;
	}
	this->update();
}

int Croft::getValue(){
	return this->value;
}

void Croft::setValue(int value, QString imageDir){
	this->value = value;
	if(this->value >0){
		this->valueImg = new QPixmap(imageDir+"/croft_"+QString::number(this->value)+".png");
	}
}

int Croft::getType(){
	return this->type;
}

void Croft::setPosition(int pos_x, int pos_y){
	this->pos_x = pos_x;
	this->pos_y = pos_y;
}

int Croft::getPositionX(){
	return this->pos_x;
}

int Croft::getPositionY(){
	return this->pos_y;
}

void Croft::setUsedBy(QObject *piece){
	if(this->usedBy != NULL)
		disconnect(this, 0, this->usedBy, 0);
	this->usedBy = piece;
	if(this->usedBy !=NULL){
		connect(this, SIGNAL(sigClickedOnCroft()), this->usedBy, SLOT(croftClicked()));
		connect(this, SIGNAL(sigPieceOut()), this->usedBy, SLOT(pieceOut()));
	}
}

void Croft::setUsedBy(QObject *piece, int player, QPixmap *playerAvatar){
	Croft::setUsedBy(piece);
	this->usedByPlayer = player;
	if(this->usedByPlayer > 0){
		this->avatarImg = playerAvatar;
	}else{
		this->avatarImg = NULL;
	}
	this->update();
}

int Croft::getOwner(){
	return this->owner;
}

void Croft::setOwner(int player){
	this->owner = player;
}

void Croft::setOwnerColor(QColor *color, int playerNumber){
	switch(this->type){
		case OUT:
			this->pen->setColor(*color);
			this->brush->setColor(*color);
		break;
		case HOME:
			this->pen->setColor(*color);
			this->brush->setColor(*color);
		break;
		case START:
			if(playerNumber == this->owner){
				this->pen->setColor(*color);
				this->pen->setWidth(3);
				this->brush->setColor(Qt::transparent);
			}
		break;
	}
	this->update();
}

void Croft::highLight(bool type, QColor *playerColor){
	if(type){
		QColor transparentColor = *playerColor;
		transparentColor.setAlpha(200);
		this->brush->setColor(transparentColor);
	}else{
		if(this->type != Croft::HOME)
			this->brush->setColor(Qt::transparent);
		else
			this->brush->setColor(*playerColor);
	}
		this->update();
}

void Croft::mousePressEvent(QMouseEvent *event){
	if(this->usedBy != NULL){
		emit sigClickedOnCroft();
	}
}

int Croft::isUsedBy(){
	return this->usedByPlayer;
}

void Croft::pieceOut(){
	if(this->usedBy != NULL){
		emit sigPieceOut();
	}
}