/** @file hra2011.h
 *  @brief Modul hry �lov��e, nezlob se!
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef HRA2011_H
#define HRA2011_H

#include <QtGui/QMainWindow>
#include <QtGui/QPlainTextEdit>
#include <QtXml>
#include <QMessageBox>
#include "ui_hra2011.h"
#include "gameboard.h"
#include "player.h"
#include "settings.h"
#include "gamesettings.h"
#include "playersettings.h"
#include "network.h"
#include "login.h"
#include "networklobby.h"
#include "dice.h"


/** 
 *  @brief Hlavn� t��da hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
class hra2011 : public QMainWindow
{
	Q_OBJECT

public:
	hra2011(QApplication *app, QWidget *parent = 0, Qt::WFlags flags = 0);
	~hra2011();

signals:
	void connected();
	void registered();
	void notreged();
	void logged();
	void wronglogin();

public slots:
	void nextPlayer();
	void nextNetPlayer();
	void playerFinishedSlot();
	void playedWith(int pieceNumber);
	void networkGameLoad(const QString &xmlFile);

private:
	Ui::hra2011Class ui;

	QHBoxLayout *mainLayout;
	QVBoxLayout *rightLayout;
	QFormLayout *computerModeLayout;
	GameBoard *gameBoard;
	QString mapFile;
	int numOfPlayers;
	QVector<Player *> players;
	QVector<QLabel *> lblPlayers;
	QVector<QComboBox *> comboComputerMode;
	QVector<QString> ranks;
	QString actualPlayerSheet;
	int onMove;
	QPlainTextEdit *textBox;
	QPushButton *nextPlayerButton;
	QString saveFile;
	void newGame(QString mapFile, int numOfPlayers, bool isNetwork = false);
	void saveGame(QString fileName);
	void gameFinish();
	QGroupBox *setMap;
	QGroupBox *setPlayers;
	QPushButton *setStartButton;
	Dice *dice;
	Settings *settingDialog;
	GameSettings *gameSettings;
	Network *networkGame;
	Login *loginNetwork;
	NetworkLobby *lobby;
	int automaticSwitchCount;
	void computerModeNext();
	QString makeSaveGame();
	void makeLoadGame(QString savedGame);
	bool isNetworkGame;

private slots:
	void newGameSlot();
	void saveGameAsSlot();
	void saveGameSlot();
	void gameSettingsSlot();
	void showManualSlot();
	void showAboutSlot();
	void newNetworkGameSlot();
	void leaveNetworkGameSlot();
	void setNetworkClass();
	void networkReceivedInfo(int onMove, int value, int lastPiece);
	void computerModeChanged(int index);

	/**
	 * @brief Na�te ulo�enou hru a spust� ji
	 *
	 * Otev�e dialog pro v�b�r souboru, nahraje soubor a p�iprav� hru
	 * @see saveGameSlot();
	 * @todo vyplneni puvodnima hodnotama
	 */
	void loadGameSlot();
};

#endif // HRA2011_H
