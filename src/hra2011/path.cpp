/** @file path.cpp
 *  @brief Modul pro pr�ci s cestou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "path.h"

Path::Path(QObject *parent)
	: QObject(parent)
{
	pathSize = 0;
}

Path::~Path()
{

}

/**
 * Prida policko na konec cesty
 * @param *croft Pridavane policko
 */
void Path::append(Croft* croft)
{
	this->path.append(croft);
	this->pathSize++;
}

/**
 * Prida policko na zacatek cesty
 * @param *croft Pridavane policko
 */
void Path::prepend(Croft* croft)
{
	this->path.prepend(croft);
	this->pathSize++;
}

/**
 * Vrati delku cesty
 * @return Delka cesty
 */
int Path::size()
{
	return this->pathSize;
}

/**
 * Vrati policko na zadane pozici v ceste.
 * @param position Pozice vraceneho policka v ceste
 * @return Vracene policko
 */
Croft* Path::at(int position){
	return this->path.at(position);
}

/**
 * Vrati offset (1D rozmer) policka podle souradnic, kde lezi.
 * @param x Pozice x
 * @param y Pozice y
 * @return offset policka
 */
int Path::getCroftOffset(int x, int y){
	int i = -1;
	Croft* temp = Path::at(0);
	while(i <this->pathSize && (temp->getPositionX() != x || temp->getPositionY() != y)){
		i++;
		temp = Path::at(i);
	}
	return i;
}

/**
 * Najde dalsi pozici policka, kam lze skocit
 * @param actualPosition Aktualni pozice (figurky), odkud se hleda dalsi
 * @param value Hodnota policka, ktere se hleda
 * @return Pozice dalsiho policka
 */
int Path::getNextJump(int actualPosition, int value){
	Croft* result;
	int i = -1;
	if(actualPosition+1 < this->pathSize){
		i = actualPosition;
		do{
			i++;
			result = this->path.at(i);	
		}while(result->getValue() != value && i+1 < this->pathSize);
		if(result->getValue() != value)
			i = -1;
	}
	return i;	
}