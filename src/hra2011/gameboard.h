/** @file gameboard.h
 *  @brief Modul pro pr�ci s hern� deskou
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QWidget>
#include <QtGui/QGridLayout>
#include "croft.h"
#include "path.h"

/** 
 *  @brief T��da obsahuj�c� sou�adnice bodu
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
class Position{
public:

	/**
	 * @brief Vytvo�� objekt a nastav� pozice na hodnotu x=0, y=0
	 */
	Position();

	/**
	 * @brief Vytvo�� objekt a nastav� zadan� pozice
	 * @param x Pozice x
	 * @param y Pozice y
	 */
	Position(int x, int y);

	/**
	 * @brief P�i�ad� hodnotu other do Position a vr�t� ukazatel na Position
	 */
	Position &operator= (const Position & other);

	/**
	 * @brief Vr�t� true, pokud je Position rovno other, jinak vr�t� false
	 * @return True, pokud je Position rovno other
	 */
	bool operator== (const Position & other) const;

	/**
	 * @brief Vr�t� true, pokud se Position nerovn� other, jinak vr�t� false
	 * @return True, pokud se Position nerovn� other
	 */
	bool operator!= (const Position & other) const;

	/**
	 * @brief Vr�t� ��slo ��dku(hodnota X).
	 * @return ��slo ��dku
	 */
	int getX();

	/**
	 * @brief Vr�t� ��slo sloupce (hodnota Y).
	 * @return ��slo sloupce sloupce
	 */
	int getY();

	/**
	 * @brief Vr�t� 1D pozici
	 * @param width ���ka matice
	 * @return Offset v matici (1D pozice)
	 */
	int getOffset(int width);

	/**
	 * @brief Nastav� nov� sou�adnice
	 * @param x Pozice x
	 * @param y Pozice y
	 */
	void set(int x, int y);

	/**
	 * @brief Vr�t� pol��ko vpravo od aktu�ln�ho
	 * @return Pozice pol��ka vpravo od aktu�ln�ho
	 */
	Position right();

	/**
	 * @brief Vr�t� pol��ko vlevo od aktu�ln�ho
	 * @return Pozice pol��ka vlevo od aktu�ln�ho
	 */
	Position left();

	/**
	 * @brief Vr�t� pol��ko nad aktu�ln�m
	 * @return Pozice pol��ka nad aktu�ln�m
	 */
	Position up();

	/**
	 * @brief Vr�t� pol��ko pod aktu�ln�m
	 * @return Pozice pol��ka pod aktu�ln�m
	 */
	Position down();
private:
	int x;
	int y;
};


/** 
 *  @brief T��da reprezentuj�c� hern� desku
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
class GameBoard : public QWidget
{
	Q_OBJECT

public:
	/**
	 * @brief Vytvo�� hern� desku
	 * @param parent Rodi�ovsk� QWidget
	 * @param mapFile Cesta k souboru s mapou 
	 * @param players Po�et hr���, kte�� budou hr�t
	 * @param imageDir Slo�ka s obr�zky
	 */
	GameBoard(QWidget *parent, QString &mapFile, int players, const QString &imageDir);

	/**
	 * @brief Destruktor hern� desky
	 */
	~GameBoard();

	/**
	 * @brief Sestav� hr��ovou cestu od pozice OUT do dome�ku
	 * @param *parent Objekt, kter� bude rodi� vytvo�en� cesty (objekt hr��e)
	 * @param player ��slo hr��e, pro kter�ho se sestavuje cesta (1-n)
	 * @return Vr�t� ukazatel na hr��ovu cestu
	 */
	Path* makePlayerPath(QObject *parent, int player);

	/**
	 * @brief Vr�t� ��st XML s nastaven�m hern� plochy
	 * @return Nastaven� hern� plochy
	 * @see hra2011::saveGame
	 */
	QString saveGameGameboard();

	/**
	 * @brief Na�te hodnoty hern�ch pol��ek
	 * @param croftValues �et�zec obsahuj�c� hodnoty
	 */
	void loadCroftValues(QString croftValues);

private:
	QGridLayout *gameBoardLayout; // grid s hracim polem
	int players; // pocet hracu
	
	/**
	* @brief Vygeneruje hrac� plochu ve tvaru k��e.
	* @param edge D�lka del�� hrany (skute�n� d�lka je o 1 v�t��)
	*/
	void renderCross(int edge);

	/**
	 * @brief Na�te hern� pl�n ze souboru a vytvo�� se�azen� cesty.
	 * @param &filePath Cesta k souboru s mapou
	 * @return Vr�t� true, pokud se povedlo vytvo�it hrac� desku, jinak false
	 *
	 * @todo QList<Croft*> pathLogicalOLD; << nejsp� nen� pot�eba
	 */
	bool loadGamePlane(const QString &filePath);
	QVector<QVector<char> > *map2D;
	int map_size_x;
	int map_size_y;
	int map_players;
	int pathSize; // delka hraciho pole
	QList<Croft*> crofts; // vsechny hraci policka nactene po radcich
	QList<Croft*> pathLogicalOLD;
	Path *pathLogical;

	QString imageDir;

	/**
	 * @brief Rekurzivn� projde cestu a s�ad� pol��ka do pathLogical.
	 * @param finish Sou�adnice c�lov�ho pol��ka - zde kon�� hled�n�.
	 * @param previous Sou�adnice p�echoz�ho pol��ka - p�ipr�chodu v kruhu je rovno c�lov�mu pol��ku.
	 * @param actual Sou�adnice aktu�ln�ho pol��ka - p�i vol�n� funkce je to startovn� pol��ko.
	 */
	void makeLogicalPath(Position finish, Position previous, Position actual);

	/**
	 * @brief Najde v �ty�okol� dal�� mo�n� skok
	 * @param type Typ hledan�ho hrac�ho pol��ka
	 * @param previous Pol��ko, odkud p�ich�z�
	 * @param actual Aktu�ln� pol��ko, kde te� stoj�
	 * @return Vrac� sou�adnice n�sleduj�c�ho mo�n�ho pol��ka
	 * @see isCroft
	 */
	Position findNextCroft(int type, Position previous, Position actual);

	/**
	 * @brief Zjist� podle znaku jestli je na�ten� bod hern� pol��ko
	 * @param type Typ kontrolovan�ho hern�ho pol��ka
	 * @param character Znak, kter� se kontroluje
	 * @return Vrac� true, pokud kontrolovan� znak pat�� do hledan� skupiny
	 */
	bool isCroft(int type, char character);

	/**
	 * @brief Generuje n�hodn� hodnoty pro hrac� pole.
	 *
	 * Kontroluje po�et v�skyt�.
	 * @return ��slo mezi 1-6
	 */
	int generateRandomNumber();
	int numOfOccur[6]; //pocet vyskytu obrazku
};



#endif // GAMEBOARD_H
