/** @file gamesettings.cpp
 *  @brief Modul s nastaven�m glob�ln�m nastaven�m hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "gamesettings.h"

GameSettings::GameSettings(QWidget *parent, QString settingsFile)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->settingsFile = settingsFile;
	loadSettings(this->settingsFile);
}

GameSettings::~GameSettings(){

}

void GameSettings::accept(){
	this->templateDir = ui.templateEdit->currentText();
	this->documentationPath = ui.documentationEdit->text();
	this->manualFile = ui.manualFileEdit->text();
	this->defaultSaveDir = ui.defaultSaveDirEdit->text();
	this->defaultGameboards = ui.defaultGameboardsEdit->text();
	this->serverAddress = ui.serverAddressEdit->text();
	this->serverPort = ui.serverPortEdit->value();
	this->automaticSwitch = ui.switchAutomaticlyCheck->isChecked();
	this->automaticSwitchNumber = ui.switchAfterEdit->value();

	GameSettings::saveSettings(this->settingsFile);
	QDialog::accept();
}

void GameSettings::on_defaultSaveDirButton_clicked(){
	ui.defaultSaveDirEdit->setText(QFileDialog::getExistingDirectory(this, tr("Select Directory with saves"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
}

void GameSettings::on_defaultGameboardsButton_clicked(){
	ui.defaultGameboardsEdit->setText(QFileDialog::getExistingDirectory(this, tr("Select Directory with gameboards"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
}

void GameSettings::on_manualFileButton_clicked(){
	ui.manualFileEdit->setText(QFileDialog::getOpenFileName(this, tr("Select help file"), "", tr("Help files (*.pdf)")));
}

void GameSettings::on_documentationButton_clicked(){
	ui.documentationEdit->setText(QFileDialog::getExistingDirectory(this, tr("Select Directory with documentation"), "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
}

QString GameSettings::getDocumentationPath(){
	return this->documentationPath;
}

QString GameSettings::getManualFile(){
	return this->manualFile;
}

QString GameSettings::getDefaultSaveDir(){
	return this->defaultSaveDir;
}

QString GameSettings::getDefaultGameboardsDir(){
	return this->defaultGameboards;
}

QString GameSettings::getServerAddress(){
	return this->serverAddress;
}

QString GameSettings::getTemplateDir(){
	return "Templates/"+this->templateDir;
}

int GameSettings::getServerPort(){
	return this->serverPort;
}

void GameSettings::loadSettings(QString &settingsFile){
	QFile file(this->settingsFile);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
		// nastaven� existuje
		QDomDocument doc("clovece"); 
		doc.setContent(&file);
		QDomElement xClovece = doc.firstChildElement("clovece"); 
		if(!xClovece.isNull()){
			// �ten� nastaven� hry
			QDomElement xSettings = xClovece.firstChildElement("setting");
			while(!xSettings.isNull()){
				if(xSettings.attribute("type") == "templateDir")
					this->templateDir = xSettings.attribute("value");
				else if(xSettings.attribute("type") == "documentationPath")
					this->documentationPath =  xSettings.attribute("value");
				else if(xSettings.attribute("type") == "manualFile")
					this->manualFile =  xSettings.attribute("value");
				else if(xSettings.attribute("type") == "defaultSaveDir")
					this->defaultSaveDir =  xSettings.attribute("value");
				else if(xSettings.attribute("type") == "defaultGameboards")
					this->defaultGameboards =  xSettings.attribute("value");
				else if(xSettings.attribute("type") == "serverAddress")
					this->serverAddress =  xSettings.attribute("value");
				else if(xSettings.attribute("type") == "serverPort")
					this->serverPort =  xSettings.attribute("value").toInt();
				else if(xSettings.attribute("type") == "automaticSwitch")
					this->automaticSwitch =  static_cast<bool>(xSettings.attribute("value").toInt());
				else if(xSettings.attribute("type") == "automaticSwitchNumber")
					this->automaticSwitchNumber =  xSettings.attribute("value").toInt();
				xSettings=xSettings.nextSiblingElement("setting");
			}
		}
		file.close();
	}else{
		//vytvo�� se defaultn� nastaven� 
		this->templateDir = "numbers";
		this->documentationPath = "Doc";
		this->manualFile = "Help/manual.pdf";
		this->defaultSaveDir = "Saves";
		this->defaultGameboards = "Gameboards";
		this->serverAddress = "127.0.0.1";
		this->serverPort = 55555;
		this->automaticSwitch = false;
		this->automaticSwitchNumber = 5;
	}	
	ui.templateEdit->setCurrentIndex(ui.templateEdit->findText(this->templateDir));
	ui.documentationEdit->setText(this->documentationPath);
	ui.manualFileEdit->setText(this->manualFile);
	ui.defaultSaveDirEdit->setText(this->defaultSaveDir);
	ui.defaultGameboardsEdit->setText(this->defaultGameboards);
	ui.serverAddressEdit->setText(this->serverAddress);
	ui.serverPortEdit->setValue(this->serverPort);
	ui.switchAfterEdit->setValue(this->automaticSwitchNumber);
	ui.switchAutomaticlyCheck->setChecked(this->automaticSwitch);

}

void GameSettings::saveSettings(QString &settingsFile){
	QFile file(settingsFile);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	// ulo�� nastaven� do souboru

	QString result;
	result = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<clovece>\n";
	result += "\t<setting type=\"documentationPath\" value=\""+this->documentationPath+"\" />\n";
	result += "\t<setting type=\"manualFile\" value=\""+this->manualFile+"\" />\n";
	result += "\t<setting type=\"defaultSaveDir\" value=\""+this->defaultSaveDir+"\" />\n";
	result += "\t<setting type=\"defaultGameboards\" value=\""+this->defaultGameboards+"\" />\n";
	result += "\t<setting type=\"serverAddress\" value=\""+this->serverAddress+"\" />\n";
	result += "\t<setting type=\"templateDir\" value=\""+this->templateDir+"\" />\n";
	result += "\t<setting type=\"automaticSwitch\" value=\""+QString::number(static_cast<int>(this->automaticSwitch))+"\" />\n";
	result += "\t<setting type=\"automaticSwitchNumber\" value=\""+QString::number(this->automaticSwitchNumber)+"\" />\n";
	result += "\t<setting type=\"serverPort\" value=\""+QString::number(this->serverPort)+"\" />\n";
	result += "</clovece>\n";

	QTextStream settingout(&file);
	 settingout << result << "\n";
	file.close();

}
bool GameSettings::getAutomaticSwitch(){
	return this->automaticSwitch;
}

int GameSettings::getAutomaticSwitchNumber(){
	return this->automaticSwitchNumber;
}