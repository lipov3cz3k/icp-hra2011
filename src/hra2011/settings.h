/** @file settings.h
 *  @brief Modul s nastaven�m instance hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QtGui/QGridLayout>
#include "ui_settings.h"
#include "playersettings.h"
#include "gamesettings.h"

/** 
 *  @brief T��da obsahuj�c� nastaven� hry
 *  @author Tom� Lipovsk�
 *  @version 0.3
 *  @date 2011-05-05
 */
class Settings : public QDialog
{
	Q_OBJECT

public:
	/**
	 * @brief Nastaven� nov� instance hry
	 * @param parent Rodi�ovsk� QWidget
	 * @param gameSettings Glob�ln� nastaven� hry
	 */
	Settings(QWidget *parent, GameSettings *gameSettings);

	/**
	 * @brief Destruktor nastaven� instance hry
	 */
	~Settings();

	/**
	 * @brief Obsahuje cestu k map�
	 */
	QString mapFilePath;

	/**
	 * @brief Vr�t� nastaven� konkr�tn�ho hr��e
	 * @param i ��slo hr��e
	 * @return Objekt s nastavne�m hr��e
	 */
	PlayerSettings* getPlayerSetting(int i);

	/**
	 * @brief P�id� nastaven� hr��e do instance hry
	 * @param playerName Jm�no hr��e
	 * @param playerType Typ hr��e (odpov�d� ��slov�n� z v��tu Player::PlayerType)
	 * @param playerColor Barva hr��e
	 * @param boardPosition Pozice hr��e na hern� desce
	 * @param computerMode Typ hern�ho m�du po��ta�e
	 */
	void addPlayer(QString playerName, int playerType, QColor *playerColor, int boardPosition, int computerMode=0);

	/**
	 * @brief Na�te nastaven� hry
	 * @param numOfPlayers Po�et hraj�c�ch hr���
	 * @param mapFile Soubor s mapou
	 * @param onMove ��slo hr��e, kter� je na tahu
	 *
	 * K na��t�n� ulo�en� hry.
	 */
	void loadSettings(int numOfPlayers, QString mapFile, int onMove);

	/**
	 * @brief Na�te do nastaven� instance hry hodnoty pol��ek
	 * @param croftValues Hodnoty pol��ek
	 */
	void loadCroftValues(QString croftValues);

	/**
	 * @brief Po�et hraj�c�ch hr���
	 */
	int playerNum;

	/**
	 * @brief Kdo byl na tahu v ulo�en� h�e
	 */
	int onMove;
	
	/**
	 * @brief Hodnoty pol��ek z ulo�en� hry
	 */
	QString croftValues;
private:
	Ui::Settings ui;
	QVector<QLineEdit *> playerNameEdit;
	QVector<QComboBox *> playerColorCombo;
	QVector<QComboBox *> playerTypeCombo;
	QVector<QComboBox *> computerModeCombo;
	QVector<PlayerSettings *> players;
	
	GameSettings *gameSettings;
	int playerNumSelect;
private slots:
		void accept();
		void loadMapSlot();

};

#endif // SETTINGS_H
