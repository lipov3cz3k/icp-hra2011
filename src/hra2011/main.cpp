/** @file main.cpp
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "hra2011.h"
#include <QtGui/QApplication>



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	hra2011 w(&a);
	w.show();
	return a.exec();
}
