/** @file networklobby.cpp
 *  @brief Modul pro lobby s�ov� hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */
#include "stdafx.h"
#include "networklobby.h"

NetworkLobby::NetworkLobby(QWidget *parent, GameSettings *gameSettings, Network *network)
	: QDialog(parent)
{
	ui.setupUi(this);
	this->gameSettings = gameSettings;
	this->network = network;
	ui.loadGameGroup->setEnabled(false);
	ui.makeGameGroup->setEnabled(false);
	ui.createGameButton->setEnabled(false);
	ui.acceptButton->setEnabled(false);


	connect(ui.loadMap, SIGNAL(clicked()), this, SLOT(loadMapSlot()));
	connect(network, SIGNAL(gameListReceived(QStringList)), this, SLOT(fillLoadListSlot(QStringList)));
	connect(network, SIGNAL(playerListReceived(QList <QPair<QString, int> >)), this, SLOT(loadPlayersSlot(QList <QPair<QString, int> >)));
	connect(network, SIGNAL(checkNewGame(const bool &)), this, SLOT(enableCreateGame(const bool &)));
	ui.mapFilePath->setText(this->gameSettings->getDefaultGameboardsDir()+"/standard_4.xml");

	this->localPlayerBoardPosition = -1;
	//this->network->getGameList();
	this->network->checkCanCreate();
	// vytvo�� tabulku pro hr��e
	this->maxPlayers = 4;
	for(int i = 0; i<this->maxPlayers; i++){
		playerNameEdit.append(new QLineEdit(this));
		playerNameEdit.last()->setReadOnly(true);
		sitButton.append(new QPushButton(tr("Sit to position ")+QString::number(i+1),this));
		sitButton.last()->setEnabled(false);
		connect(sitButton.last(), SIGNAL(clicked()), this, SLOT(sitSlot()));
		ui.playerBox->addWidget(playerNameEdit.last(), i, 1);
		ui.playerBox->addWidget(sitButton.last(), i, 2);
	}
	
	
}

NetworkLobby::~NetworkLobby()
{

}

void NetworkLobby::on_loadGameGroup_toggled(bool state){
	ui.makeGameGroup->setChecked(!state);
}

void NetworkLobby::on_makeGameGroup_toggled(bool state){
	ui.loadGameGroup->setChecked(!state);
}

void NetworkLobby::on_actualizeLoadButton_clicked(){
	this->network->getGameList();
}
void NetworkLobby::loadMapSlot(){
	ui.mapFilePath->setText(QFileDialog::getOpenFileName(this, tr("Select map"),  QDir::currentPath()+"\\"+this->gameSettings->getDefaultGameboardsDir()+"\\", tr("Map files (*.xml)")));
}

void NetworkLobby::fillLoadListSlot(const QStringList &gameList){
	ui.loadCombo->clear();
	ui.loadCombo->addItems(gameList);
}

void NetworkLobby::on_createGameButton_clicked(){
	if(ui.loadGameGroup->isChecked()){
		// bude se na��tat ulo�en� hra

	}else{
		// bude se na��tat nov� hra
		network->newNetGame(this->maxPlayers);		
	}

	this->localPlayerBoardPosition = 1;
	// zp��stupn� pozice hr���
	for(int i = 0; i<this->maxPlayers; i++){
		playerNameEdit.at(i)->setEnabled(true);
	}
}

void NetworkLobby::sitSlot(){

	QObject *obj = this->sender();
	
	QPushButton *button =  qobject_cast<QPushButton *>(obj);
	int x = button->text().mid(button->text().size()-1).toInt();

	if(localPlayerBoardPosition == -1){
		network->joinGame(x);
		this->localPlayerBoardPosition = x;
		button->setText(tr("Leave position ")+QString::number(x));
	}else{
		network->leaveGame();
		this->localPlayerBoardPosition = -1;
		button->setText(tr("Sit to position ")+QString::number(x));
	}
}


void NetworkLobby::loadPlayersSlot(const QList <QPair<QString, int> > playerList){
	for(int i = 0; i < playerNameEdit.size(); i++){
		playerNameEdit.at(i)->clear();
		if(localPlayerBoardPosition == -1 && localPlayerBoardPosition != 1)
			sitButton.at(i)->setEnabled(true);
		else
			sitButton.at(i)->setEnabled(false);
	}
	this->connectedPlayers = playerList.size();

	if(this->connectedPlayers > 0){
		ui.loadGameGroup->setEnabled(false);
		ui.makeGameGroup->setEnabled(false);
	}else{
		ui.loadGameGroup->setEnabled(true);
		ui.makeGameGroup->setEnabled(true);
	}

	for(int i = 0; i < this->connectedPlayers ; i++){
		QString name = playerList.at(i).first;
		int position = playerList.at(i).second;
		playerNameEdit.at(position-1)->setText(name);
		if(this->localPlayerBoardPosition != position || position == 1)
			sitButton.at(position-1)->setEnabled(false);
		else
			sitButton.at(position-1)->setEnabled(true);
	}
	if(this->connectedPlayers > 1 && this->localPlayerBoardPosition == 1){
	// dostatek hr��� - povol� spu�t�n�
		ui.acceptButton->setEnabled(true);
	}else{
		ui.acceptButton->setEnabled(false);
	}



}
void NetworkLobby::enableCreateGame(const bool &canCreate){
	if(canCreate){
		ui.loadGameGroup->setEnabled(true);
		ui.makeGameGroup->setEnabled(true);
		ui.createGameButton->setEnabled(true);
		for(int i = 0; i<this->maxPlayers; i++){
			playerNameEdit.at(i)->setEnabled(false);
			sitButton.at(i)->setEnabled(false);
		}
	}else{
	// zp��stupn� pozice hr���
		ui.loadGameGroup->setEnabled(false);
		ui.makeGameGroup->setEnabled(false);
		for(int i = 0; i<this->maxPlayers; i++){
			playerNameEdit.at(i)->setEnabled(true);
			sitButton.at(i)->setEnabled(true);
			ui.acceptButton->setText("Creator starts game.");
		}


	}

}

PlayerSettings* NetworkLobby::getPlayerSetting(int i){
	return this->players.at(i);
}

void NetworkLobby::on_acceptButton_clicked(){
	if(this->localPlayerBoardPosition == 1){
		// je zakladatel hry
		// ulo�� si nastaven�
		this->mapFilePath = this->ui.mapFilePath->text();

		for(int i = 0; i < this->maxPlayers; i++){
			if(!this->playerNameEdit.at(i)->text().isEmpty()){
				int r = ((i+1) * 128)%255;
				int g = ((i+1) * 32)%255;
				int b = ((i+1) * 64)%255;
				QColor *playerColor = new QColor(r, g, b);
				this->players.append(new PlayerSettings(this, this->playerNameEdit.at(i)->text(), Player::LAN, playerColor, i+1, 0));
			}
		}

		QDialog::accept();
	}
}

void NetworkLobby::on_cancelButton_clicked(){
	// logout


}