/** @file gamesettings.h
 *  @brief Modul s nastaven�m glob�ln�m nastaven�m hry
 *  @authors Tom� Lipovsk�
 *  @authors Tom� Ondrouch
 *  @version 0.3
 *  @date 2011-05-05
 */

#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H

#include <QObject>
#include <QtXml>
#include "ui_gamesettings.h"


/** 
 *  @brief T��da obsahuj�c� glob�ln� nastaven� hry
 *  @author Tom� Lipovsk�
 *  @version 0.3
 *  @date 2011-05-05
 */
class GameSettings : public QDialog
{
	Q_OBJECT

public:
	/**
	 * @brief Na�te nastaven� hry
	 * @param parent Rodi�ovsk� QWidget
	 * @param settingsFile soubor s nastaven�,
	 *
	 * V p��pad� �e soubor s nastaven�m neexistuje, vytvo�� se nov� s defaultn�ma hodnotama.
	 */
	GameSettings(QWidget *parent = 0, QString settingsFile = "");

	/**
	 * @brief Destruktor hern�ho nastaven�
	 */
	~GameSettings();

	/**
	 * @brief Vr�t� cestu k adres��i s dokumentac�
	 * @return Adres�� s dokumentac�
	 */
	QString getDocumentationPath();

	/**
	 * @brief Vr�t� cestu k manu�lov�mu souboru
	 * @return Manu�lov� soubor
	 */
	QString getManualFile();

	/**
	 * @brief Vr�t� cestu k adres��i s ulo�en�mi hrami
	 * @return Adres�� s hrami
	 */
	QString getDefaultSaveDir();

	/**
	 * @brief Vr�t� cestu k adres��i mapami
	 * @return Adres�� s mapami
	 */
	QString getDefaultGameboardsDir();

	/**
	 * @brief Vr�t� adresu serveru
	 * @return Adresa serveru
	 */
	QString getServerAddress();

	/**
	 * @brief Vr�t� cestu k adres��i s �ablonami
	 * @return Adres�� s �ablonami
	 */
	QString getTemplateDir();

	/**
	 * @brief Vr�t� ��slo portu serveru
	 * @return ��slo portu
	 */
	int getServerPort();

	/**
	 * @brief Vr�t� true, pokud je zapnut� automatick� p�ep�n�n� hern�ho m�du po��ta�e
	 * @return Automatick� p�ep�n�n� m�du
	 */
	bool getAutomaticSwitch();

	/**
	 * @brief Vr�t� po�et kol, po kter�ch se p�epne hern� m�d po��ta�e
	 * @return Po�et kol
	 */
	int getAutomaticSwitchNumber();

private:
	Ui::GameSettings ui;
	QString documentationPath;
	QString manualFile;
	QString defaultSaveDir;
	QString defaultGameboards;
	QString serverAddress;
	QString templateDir;
	QString settingsFile;
	bool automaticSwitch;
	int automaticSwitchNumber;
	int serverPort;
	void loadSettings(QString &file);
	void saveSettings(QString &file);

private slots:
	void accept();
	void on_defaultSaveDirButton_clicked();
	void on_defaultGameboardsButton_clicked();
	void on_manualFileButton_clicked();
	void on_documentationButton_clicked();
};

#endif // GAMESETTINGS_H
